﻿using Backend;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    [SerializeField] FakeTile fakeTile;
    MapData mapData = new MapData();

    [Header("Map")]
    [SerializeField] int height; 
    [SerializeField] int width;

    [SerializeField] float manualYOffset;

    void Awake()
    {
        print("Save - space /// n - new map /// l - load current map // q - clear data");

        //CreateNewMap();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            SaveCurrentMap();
            print("Saving");
        }
        else if (Input.GetKeyDown(KeyCode.N))
        {
            ClearMap();
            CreateNewMap();
            print("Generating");
        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            ClearMap();
            LoadCurrentMap();
            print("Loading");
        }
        else if (Input.GetKeyDown(KeyCode.Q))
        {
            DataStorage.ProgramData.MapData = null;
            SaveProgData();
            print("Clearing");
        }
    }

    private void ClearMap()
    {
        var root = GameObject.Find(" ~ Dev hex map ~ ");
        if(root != null)
        {
            Destroy(root);
            mapData = null;
        }
    }

    void CreateNewMap()
    {
        mapData = new MapData();

        mapData.Height = height;
        mapData.Width = width;

        mapData.Tiles = new List<TileData>();

        var root = new GameObject(" ~ Dev hex map ~ ").transform;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                mapData.Tiles.Add(new TileData() { X = x, Y = y, Depth = 1});
                SetupTile(x, y, 1, root);
            }
        }
    }

    void SaveCurrentMap()
    {
        //Initialize
        var progData = Resources.Load<TextAsset>("Program-Data").text;
        DataStorage.Load(null, progData);

        var fakeTiles = FindObjectsOfType<FakeTile>();
        mapData.Tiles = fakeTiles
                        .Select(x => new TileData() { X = x.X, Y = x.Y, Depth = x.Depth })
                        .ToList(); ;

        DataStorage.ProgramData.MapData = mapData;
        SaveProgData();
    }

    void SaveProgData()
    {
        var path = @"Assets\Data\Resources\Program-Data.xml";
        DataStorage.Save(path, DataStorage.ProgramData);
    }

    void LoadCurrentMap()
    {
        var progData = Resources.Load<TextAsset>("Program-Data").text;
        DataStorage.Load(null, progData);
        
        mapData = DataStorage.ProgramData.MapData;
        var root = new GameObject(" ~ Dev hex map ~ ").transform;

        foreach (var x in mapData.Tiles)
        {
            SetupTile(x.X, x.Y, x.Depth, root);
        }
    }

    void SetupTile(int x, int y, int depth, Transform root)
    {
        var tile = GameObject.Instantiate(fakeTile);

        tile.X = x;
        tile.Y = y;
        tile.Depth = depth;

        tile.gameObject.name = string.Format("Hex tile ({0}, {1})", x, y);

        tile.transform.parent = root.transform;
        tile.transform.position = GenerateCoordinates(x, y);
        
        tile.SetMaterial();
    }

    Vector3 GenerateCoordinates(int x, int y)
    {
        var spacing = MapHexManager.Spacing;

        var yOffset = x % 2 == 0 ? 0 : -spacing / 2;
        return new Vector3(x * spacing, manualYOffset, y * spacing + yOffset);
    }
}
