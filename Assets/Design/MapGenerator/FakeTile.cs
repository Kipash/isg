﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeTile : MonoBehaviour
{
    public int X = -1;
    public int Y = -1;
    [Range(0,3)] public int Depth = -1;

    [SerializeField] Color gap;
    [SerializeField] Color normal;
    [SerializeField] Color hill;
    [SerializeField] Color blocked;

    Renderer r;
    Renderer renderer
    {
        get
        {
            if (r == null)
                r = GetComponentInChildren<Renderer>();

            return r;
        }
    }

    void Update()
    {
        SetMaterial();
    }

    public void SetMaterial()
    {
        var mat = renderer.material;
        var colors = new Color[] { gap, normal, hill, blocked };

        mat.SetColor("_TintColor", colors[Depth]);

        renderer.material = mat;
    }
}
