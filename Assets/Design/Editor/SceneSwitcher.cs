﻿using UnityEditor.SceneManagement;
using UnityEditor;

public class SceneSwitcher
{
    [MenuItem("Tools/Game scenes/Pre scene")]
    public static void LoadPreScene()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/App/Scenes/PreScene.unity");
    }
    [MenuItem("Tools/Game scenes/Menu")]
    public static void LoadMenu() 
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/App/Scenes/Menu.unity");
    }
    [MenuItem("Tools/Game scenes/Main")]
    public static void LoadMain()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/App/Scenes/Main.unity");
    }

    // --- 

    [MenuItem("Tools/Dev scenes/Hexmap editor")]
    public static void LoadHexmap()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Design/MapGenerator/Scenes/Editor.unity");
    }
}