﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PathFind;

namespace Model
{
    public class Tile : SpacialObject, IHasNeighbours<Tile>
    {
        public Tile(int x, int y)
            : base(x, y)
        {
            Depth = 1;
        }
   
        //gap, plane, hill
        public int[] Occupancy = new int[3] { -1, -1, -1 };
        public int Depth { get; set; }

        /// <summary>
        /// Nastaví obsazení na uričité hloubce.
        /// </summary>
        /// <param name="depth">Hloubka kterou nastavujeme. 1-2</param>
        /// <param name="id"><c>-1</c> pokud chceme vypnout kolize, <c>0-999</c> pro nastavení ID entity co by tam stála</param>
        public void SetOccupancy(int depth, int id)
        {
            if (depth < Occupancy.Length && depth >= 0)
            {
                Occupancy[depth] = id;
            }
        }
        public bool GetOccupy(int depth, int ID)
        {
            if (depth < Occupancy.Length && depth >= 0)
                return Occupancy[depth] != -1 ? Occupancy[depth] != ID : false;
            else
                return false;
        }

        public IEnumerable<Tile> AllNeighbours { get; set; }
        
        public IEnumerable<Tile> GetNeighbours(int depth, int id)
        {
            switch (depth)
            {
                case 1:
                    return AllNeighbours.Where(x => x.Depth == 1 && !x.GetOccupy(1, id));
                case 2:
                    return AllNeighbours.Where(x => (x.Depth == 0) ||
                                                    (x.Depth == 1) ||
                                                    (x.Depth == 2 && !x.GetOccupy(2, id)));
                default:
                    return new Tile[0];
            }      
        }

        public void FindNeighbours(Tile[,] gameBoard)
        {
            var neighbours = new List<Tile>();

            var possibleExits = X % 2 == 0 ? EvenNeighbours : OddNeighbours;

            foreach (var vector in possibleExits)
            {
                var neighbourX = X + vector.X;
                var neighbourY = Y + vector.Y;

                if (neighbourX >= 0 && neighbourX < gameBoard.GetLength(0) && neighbourY >= 0 && neighbourY < gameBoard.GetLength(1))
                    neighbours.Add(gameBoard[neighbourX, neighbourY]);
            }

            AllNeighbours = neighbours;
        }

        public static List<Point> EvenNeighbours
        {
            get
            {
                return new List<Point>
                {
                    new Point(0, 1),
                    new Point(1, 1),
                    new Point(1, 0),
                    new Point(0, -1),
                    new Point(-1, 0),
                    new Point(-1, 1),
                };
            }
        }

        public static List<Point> OddNeighbours
        {
            get
            {
                return new List<Point>
                {
                    new Point(0, 1),
                    new Point(1, 0),
                    new Point(1, -1),
                    new Point(0, -1),
                    new Point(-1, 0),
                    new Point(-1, -1),
                };
            }
        }
    }
}