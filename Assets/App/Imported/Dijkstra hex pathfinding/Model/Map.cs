﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Map
    {
        public Tile[,] GameBoard;

        public int Width;
        public int Height;
        
        public Map(int width, int height)
        {
            Width = width;
            Height = height;

            InitialiseGameBoard();
        }
        
        private void InitialiseGameBoard()
        {
            GameBoard = new Tile[Width, Height];

            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    GameBoard[x, y] = new Tile(x, y);
                }
            }

            AllTiles.ToList().ForEach(o => o.FindNeighbours(GameBoard));
        }

        public IEnumerable<Tile> AllTiles
        {
            get
            {
                for (var x = 0; x < Width; x++)
                    for (var y = 0; y < Height; y++)
                        yield return GameBoard[x, y];
            }
        }

    }
}

