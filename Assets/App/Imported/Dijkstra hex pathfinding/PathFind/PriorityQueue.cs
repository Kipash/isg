﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PathFind
{
    class PriorityQueue<P, V>
    {
        private readonly SortedDictionary<P, Queue<V>> list = new SortedDictionary<P, Queue<V>>();

        int count;

        public void Enqueue(P priority, V value)
        {
            count++;

            Queue<V> q;
            if (!list.TryGetValue(priority, out q))
            {
                q = new Queue<V>();
                list.Add(priority, q);
            }
            q.Enqueue(value);
        }

        public V Dequeue()
        {
            count--;

            var pair = list.First(); //Getting a element with lowest number
            var v = pair.Value.Dequeue();
            if (pair.Value.Count == 0) // nothing left of the top priority.
                list.Remove(pair.Key);
            return v;
        }

        public bool IsEmpty
        {
            get { return count == 0; }
        }
    }
}
