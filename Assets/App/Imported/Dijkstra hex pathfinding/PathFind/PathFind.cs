﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PathFind
{
    public static class PathFind
    {
        const int MaxIterations = 5000;


        public static Path<Node> FindPath<Node>(
            Node start,
            Node destination,
            int depth,
            int id,
            Func<Node, Node, double> distance,
            Func<Node, double> estimate)
            where Node : IHasNeighbours<Node>
        {
            var closed = new HashSet<Node>();
            var queue = new PriorityQueue<double, Path<Node>>();
            queue.Enqueue(0, new Path<Node>(start));

            for (int i = 0; !queue.IsEmpty; i++)
            {
                var path = queue.Dequeue();

                if (i > MaxIterations)
                {
                    UnityEngine.Debug.LogWarning("Estimating path");
                    return path;
                }

                if (closed.Contains(path.LastStep))
                    continue;
                if (path.LastStep.Equals(destination))
                    return path;

                closed.Add(path.LastStep);

                foreach (Node n in path.LastStep.GetNeighbours(depth, id))
                {
                    double d = distance(path.LastStep, n);
                    var newPath = path.AddStep(n, d);
                    queue.Enqueue(newPath.TotalCost + estimate(n), newPath);
                }
            }

            return null;
        }
    }
}