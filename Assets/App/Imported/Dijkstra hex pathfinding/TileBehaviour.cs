﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using Model;

public class TileBehaviour : MonoBehaviour
{
    public Tile Tile;

    [SerializeField] Color gap;
    [SerializeField] Color normal;
    [SerializeField] Color hill;
    [SerializeField] Color blocked;

    Renderer r;
    Renderer renderer
    {
        get
        {
            if (r == null)
                r = GetComponentInChildren<Renderer>();

            return r;
        }
    }

    //Debug
    private void OnDrawGizmos()
    {
        var normal = Tile.Occupancy[1] != -1;
        var air = Tile.Occupancy[2] != -1;

        if (normal && air)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireCube(transform.position, Vector3.one / 2);
        }
        else if (air)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(transform.position, Vector3.one / 2);
        }
        else if (normal)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(transform.position, Vector3.one / 2);
        }
    }

    public void SetMaterial()
    {
        var mat = renderer.material;

        var colors = new Color[] { gap, normal, hill, blocked };

        mat.SetColor("_TintColor", colors[Tile.Depth]);

        renderer.material = mat;
    }
}
