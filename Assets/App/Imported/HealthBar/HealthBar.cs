﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] Renderer bar;
    Transform target;

    int hp;
    int defaultHP;

    void OnEnable() 
    {
        if(GameServices.Inst)
            target = GameServices.Inst.ControlsManager.HeadTarget.transform;
    }
    public void SetDefaultHealth(int h) {
        defaultHP = h;
        SetHealth(h);
    }

    public void SetHealth(int h) 
    {
        hp = h;
    }
    
    void Update()
    {
        transform.LookAt(2 * transform.position - target.position);

        bar.material.SetFloat("_Value", (float)hp / (float)defaultHP);
    }
}
