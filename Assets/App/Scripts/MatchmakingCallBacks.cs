﻿using System;
using System.Collections.Generic;
using Photon.Realtime;

public class MatchmakingCallBacks : IMatchmakingCallbacks
{
    public Action _OnCreatedRoom;
    public Action<short, string> _OnCreateRoomFailed;
    public Action<List<FriendInfo>> _OnFriendListUpdate;
    public Action _OnJoinedRoom;
    public Action<short, string> _OnJoinRandomFailed;
    public Action<short, string> _OnJoinRoomFailed;
    public Action _OnLeftRoom;

    public void OnCreatedRoom()
    {
        if(_OnCreatedRoom != null)
            _OnCreatedRoom.Invoke();
    }

    public void OnCreateRoomFailed(short returnCode, string message)
    {
        if (_OnCreateRoomFailed != null)
            _OnCreateRoomFailed.Invoke(returnCode, message);
    }

    public void OnFriendListUpdate(List<FriendInfo> friendList)
    {
        if (_OnFriendListUpdate != null)
            _OnFriendListUpdate.Invoke(friendList);
    }

    public void OnJoinedRoom()
    {
        if (_OnJoinedRoom != null)
            _OnJoinedRoom.Invoke();
    }

    public void OnJoinRandomFailed(short returnCode, string message)
    {
        if (_OnJoinRandomFailed != null)
            _OnJoinRandomFailed.Invoke(returnCode, message);
    }

    public void OnJoinRoomFailed(short returnCode, string message)
    {
        if (_OnJoinRoomFailed != null)
            _OnJoinRoomFailed.Invoke(returnCode, message);
    }

    public void OnLeftRoom()
    {
        if (_OnLeftRoom != null)
            _OnLeftRoom.Invoke();
    }
}
