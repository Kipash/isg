﻿using UnityEngine;
using System.Collections;

public static class UnityExtensions
{
    static Vector3 defaultV3 = new Vector3(-100, -100, -100);
    public static bool IsDefault(this Vector3 v3)
    {
        return (v3.x == defaultV3.x && v3.y == defaultV3.y && v3.z == defaultV3.z);
    }
    public static Vector3 GetDefault(this Vector3 v3)
    {
        return defaultV3;
    }
}
