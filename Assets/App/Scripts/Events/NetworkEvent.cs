﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NetworkEvent
{
    //possible range 0 - 199

    none = -1,
    GameReady = 1,
    
    EntityCreated   = 50,
    EntityDemanded  = 51,
    EntityUpdated   = 52,
    EntityDestroied = 53,

    DemandAreaDamage = 90,
    AreaDamage = 91,

    EntityBuffed = 100,
    EntityBuffDemanded = 101,

    VRPlayerUpdated = 150,
}
