﻿using ExitGames.Client.Photon;
using Photon.Realtime;
using System;

public class PhotonEvents : IOnEventCallback
{
    Action<EventData> callback;
    public PhotonEvents(Action<EventData> callback)
    {
        this.callback = callback;
    }

    public void OnEvent(EventData photonEvent)
    {
        callback(photonEvent);
    }
}