﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstraktní základ pro každou entitu.
/// Lokální synchronizace v rámci entity, navzájem si entita může posílat updaty do jiných klientů. Jinak se bude chovat u mastera.
/// </summary>
public abstract class Entity : MonoBehaviour
{
    /// <summary>
    /// Tickrate Tick funkce.
    /// </summary>
    public const float TickRate = 1f / 16f;
    /// <summary>
    /// Názdev Tick funkce.
    /// </summary>
    public const string TickFunctionName = "Tick";

    /// <summary>
    /// Hloubka v které je.
    /// </summary>
    [Range(-1, 3)] public int Depth = 1;

    /// <summary>
    /// Tým ve které je.
    /// </summary>
    [Header("Entity")]
    [HideInInspector] public Team Team;
    /// <summary>
    /// Nepřátlský tým.
    /// </summary>
    public Team InvertedTeam {
        get {
            if (Team == Team.blu)
                return Team.red;
            else if (Team == Team.red)
                return Team.blu;
            else if (Team == Team.none)
                return Team.none;
            else
                throw new Exception("Unknown Team");
        }
    }

    /// <summary>
    /// Typ který je.
    /// </summary>
    public EntityType Type;
    /// <summary>
    /// Kategorie která je.
    /// </summary>
    public EntityClass Class;

    /// <summary>
    /// Zdali se dá IEntityObservrem targetovat.
    /// </summary>
    public bool Targetable = true;

    /// <summary>
    /// Zdali žije a je tedy aktivní.
    /// </summary>
    [HideInInspector] public bool Active;
    /// <summary>
    /// Zdali byl prve inicializovaný.
    /// </summary>
    protected bool Initialized;

    //Reference na managery ...
    protected EventManager eventManager { get { return GameServices.Inst.EventManager; } }
    protected EntityManager entityManager { get { return GameServices.Inst.EntityManager; } }

    /// <summary>
    /// Lokání životy. Nemohou být upraveny veřejně.
    /// </summary>
    int hp = -1;
    public int HP
    {
        get { return hp; }
        protected set { hp = value; }
    }

    /// <summary>
    /// Životy co se nastaví na každé spawnutí.
    /// </summary>
    [SerializeField] int DefaultHP;

    /// <summary>
    /// Základní implementace reagování na ztrátu životů. Pokud nemáme životy, nechá se odebrat.
    /// </summary>
    /// <param name="amount">Počet kolik hp bylo odebráno.</param>
    protected virtual void OnDamageDelt(int amount)
    {
        //TODO: Add death animation, death state.
        if (HP <= 0 && PhotonNetwork.IsMasterClient)
            entityManager.DestroyEntity(ID);
    }

    /// <summary>
    /// Základní implementace ubírání životů.
    /// </summary>
    /// <returns><c>true</c>, Zemřel, <c>false</c> nezemřel.</returns>
    /// <param name="amount">Počet kolik hp odebrat.</param>
    public virtual bool DealDamage(int amount)
    {
        hp -= amount;

        OnDamageDelt(amount);
        return HP <= 0;
    }

    /// <summary>
    /// Získání damage per second.
    /// </summary>
    public abstract float GetDPS();

    /// <summary>
    /// Úroveň ohroženi, čím menší je, tím je více nebezečnější.
    /// </summary>
    public abstract float GetThreadLevel();  

    /// <summary>
    /// Update entity, protože nechceme aby se volala každý frame, tak jí spouštíme sami.
    /// </summary>
    protected abstract void Tick();

    /// <summary>
    /// Nastavení životů apod.
    /// </summary>
    public virtual void Initialize()
    {
        gameObject.SetActive(true);
        Initialized = true;
        Active = true;
        hp = DefaultHP;
    }
    /// <summary>
    /// Usmrcení a připravení pro to, aby se entita vrátila  do poolu.
    /// </summary>
    public virtual void Deactivate()
    {
        Active = false;
        gameObject.SetActive(false);
        ID = -1;

        AppServices.Inst.EntityPool.DeactivateEntity(this);

        if (IsInvoking(TickFunctionName))
            CancelInvoke(TickFunctionName);
    }

    /// <summary>
    /// Inicializace logiky co by měla běžet na masteru.
    /// </summary>
    public virtual void MasterLogic()
    {
        var offset = UnityEngine.Random.Range(TickRate / 8, TickRate / 4);
        InvokeRepeating(TickFunctionName, offset, TickRate);
    }
    /// <summary>
    /// Inicializace logiky co by měla běžet na regularu.
    /// </summary>
    public abstract void RegularLogic();

    /// <summary>
    /// ID entity.
    /// </summary>
    public int ID = -1;
    /// <summary>
    /// statickej inkrementální počet ID, je použit pro generaci unikátního ID.
    /// </summary>
    static int idCounter;

    /// <summary>
    /// Vygeneruje unikátní ID.
    /// </summary>
    /// <returns>Unikátní ID.</returns>
    public int GetUniqueID()
    {
        return ++idCounter;
    }

    /// <summary>
    /// Calbacky pro eventy v rámci jedné entity napříč spuštěnými klienty.
    /// </summary>
    protected Dictionary<int, Action<object>> updateCallbacks = new Dictionary<int, Action<object>>();

    /// <summary>
    /// Přidá callback pro daný event. Zavolá se, jakmile lokální klient event dostane.
    /// </summary>
    /// <param name="id">Název eventu</param>
    /// <param name="callback">Callback</param>
    protected void RegisterCallBack(int id, Action<object> callback)
    {
        if (!updateCallbacks.ContainsKey(id))// && Active)
            updateCallbacks.Add(id, callback);
    }

    /// <summary>
    /// Polsat event stejné entitě na cizích klientech.
    /// </summary>
    /// <param name="data">Data s ID a payloadem.</param>
    protected void SendUpdate(EntityUpdateData data)
    {
        if(data.EntityID != -1)
            GameServices.Inst.EventManager.Raise(NetworkEvent.EntityUpdated, data);
    }

    /// <summary>
    /// Volaná z venku, pokud lokální klient dostane event pro toto ID entity, zavolá tuto funkci.
    /// Předá jí data a zavolá se patřičný callback.
    /// </summary>
    /// <param name="data">Data s ID a payloadem.</param>
    public void UpdateEntity(EntityUpdateData data)
    {
        if (updateCallbacks.ContainsKey(data.UpdateID))
            updateCallbacks[data.UpdateID].Invoke(data.Payload);
        else if(!Active)
            Debug.LogError("Receiving updates for disabled unit");
        else
            Debug.LogErrorFormat("({0}) Unregistered update callback: {1}", gameObject.name, data.UpdateID);
    }
}
