﻿using UnityEngine;
using System.Collections;

public class AreaSpectre : Spectre
{
    [SerializeField] AreaObserver areaObserver;
    [SerializeField] int Damage;
    [SerializeField] ParticleSystem particles;

    bool used;

    public override void MasterLogic()
    {
        areaObserver.EntityManager = entityManager;
        areaObserver.Depth = Depth;
        used = false;

        base.MasterLogic();
    }

    public override void RegularLogic()
    {
        
    }

    protected override void Tick()
    {
        if(!used)
        {
            Use();
            used = true;
        }
    }

    void Use()
    {
        var targets = areaObserver.GetTargets(transform.position);
        if(targets != null && targets.Length > 0)
        {
            particles.Play();

            for (int i = 0; i < targets.Length; i++)
            {
                targets[i].DealDamage(Damage);
            }
        }

        Invoke("Disable", 3);
    }

    void Disable()
    {
        entityManager.DestroyEntity(ID);
    }
}
