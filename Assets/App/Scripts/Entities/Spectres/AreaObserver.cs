﻿using System;
using System.Linq;
using UnityEngine;

[Serializable]
public class AreaObserver : IEntityObeserver
{
    public float Range;
    //[SerializeField] EntityClass[] classes;
    //public Team Team;

    public int Depth { get; set; }

    [HideInInspector] public EntityManager EntityManager;

    public Entity[] GetTargets(Vector3 currentPosition)
    {
        Debug.LogFormat("entities: {0}", EntityManager.Entities.Values.Count);
        var filtered = EntityManager.Entities.Values
                        .Where(x => //classes.Contains(x.Class) &&
                                    //x.Team == Team || (Team == Team.both && (x.Team == Team.blu || x.Team == Team.red)) && //TODO: not
                                    Range > Vector3.Distance(currentPosition, x.transform.position));
        Debug.LogFormat("filtered entities: {0}", filtered.Count());
        return filtered.ToArray();    
    }
}