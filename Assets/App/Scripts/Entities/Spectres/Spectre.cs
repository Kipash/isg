﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spectre : Entity
{
    public IEntityObeserver obeserver;

    public override void Initialize()
    {
        base.Initialize();
    }

    public override void MasterLogic()
    {
        base.MasterLogic();
    }
    
    public override float GetDPS()
    {
        return 0;
    }

    public override float GetThreadLevel()
    {
        return 0;
    }
}
