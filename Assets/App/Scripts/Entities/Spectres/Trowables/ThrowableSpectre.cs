﻿using UnityEngine;
using System;
using System.Collections;

public class ThrowableSpectre : Spectre
{
    enum callBacks { none = 0, Launch = 1, Mount = 2 }

    [SerializeField] AreaObserver areaObserver;
    [SerializeField] int damage;
    
    [Header("Projectile")]
    [SerializeField] Rigidbody rb;
    [SerializeField] float speed = 1;
    
    [Header("Reference")]
    [SerializeField] Transform projectileTip;
    
    bool interact;

    void OnEnable()
    {
        rb.isKinematic = true;
        interact = false;
        if (GameServices.Inst != null)
            areaObserver.EntityManager = GameServices.Inst.EntityManager;
    }

    void Update()
    {
        if (interact)
            transform.rotation = Quaternion.LookRotation(rb.velocity.normalized, Vector3.up);
    }

    public void OnDrawGizmos()
    {
        if (areaObserver != null)
        {
            var r = areaObserver.Range;
            var c = Color.magenta;
            Gizmos.color = new Color(c.r, c.g, c.b, 0.1f);
            Gizmos.DrawWireSphere(projectileTip.position, r);
        }
    }

    public void Mount(object payload)
    {
        var data = (MountData)payload;
        GameServices.Inst.VRPlayerManager.Mount(transform, data.Hand, data.Fake);
    }
    public void Mount(int hand)
    {
        GameServices.Inst.VRPlayerManager.Mount(transform, hand, false);
        SendUpdate(new EntityUpdateData()
        {
            EntityID = ID,
            Payload = new MountData() { Fake = true, Hand = hand },
            UpdateID = (int)callBacks.Mount,
        });
    }

    public void Launch(Vector3 dir, Vector3 pos = new Vector3(), bool sync = true)
    {
        var finalDir = dir.normalized * speed;

        transform.SetParent(null);

        if (pos != Vector3.zero)
            transform.position = pos;

        rb.isKinematic = false;
        rb.WakeUp();
        rb.AddForce(finalDir, ForceMode.Impulse);
        interact = true;

        if (sync)
        {
            SendUpdate(new EntityUpdateData()
            {
                EntityID = ID,
                Payload = new LaunchData()
                {
                    Direction = new V3(finalDir),
                    Origin = new V3(transform.position),
                },
                UpdateID = (int)callBacks.Launch
            });
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (interact)
        {
            rb.isKinematic = true;
            Invoke("Disable", 2);

            interact = false;

            Explode();
        }
    }

    void Explode()
    {
        var targets = areaObserver.GetTargets(projectileTip.position);
        Debug.LogFormat("ThrowableSpectre: found {0} targets.", targets.Length);

        foreach(var t in targets)
        {
            t.DealDamage(damage);
        }
    }

    void Disable()
    {
        gameObject.SetActive(false);
        AppServices.Inst.EntityPool.DeactivateEntity(this);
    }

    protected override void Tick()
    {
        
    }

    public override void Initialize()
    {
        base.Initialize();

        RegisterCallBack((int)callBacks.Launch, (x) => 
        {
            var data = (LaunchData)x;
            Launch(data.Direction.Vector, data.Origin.Vector, false);
        });
        RegisterCallBack((int)callBacks.Mount, Mount);
    }
    public override void MasterLogic()
    {
        base.MasterLogic();
    }

    public override void RegularLogic()
    {

    }
}

[Serializable]
public class MountData
{
    public int Hand = -1;
    public bool Fake;
}

[Serializable]
public class LaunchData
{
    public V3 Origin;
    public V3 Direction;
}