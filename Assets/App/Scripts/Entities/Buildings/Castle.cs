﻿using UnityEngine;
using System.Collections;

public class Castle : Building
{
    enum callbacks { none = 0, GetHit = 1 }

    [SerializeField] GameObject teamBlu;
    [SerializeField] GameObject teamRed;

    [SerializeField] BuildingObserver buildingObserver;
    [SerializeField] BuildingAttack buildingAttack;

    [SerializeField] HealthBar healthBar;

    public override float GetDPS()
    {
        if (attack.Delay == 0 || attack.Damage == 0)
            return 0;
        else
            return attack.Damage / attack.Delay;
    }

    public override float GetThreadLevel()
    {
        if (GetDPS() == 0f)     //If there is no DPS we need to assign some default value due to division by 0 error
            return 0.02f;
        else
            return Mathf.Sqrt(HP) / GetDPS();
    }

    public override void Initialize()
    {
        base.Initialize();

        healthBar.SetDefaultHealth(HP);


        if (Team == Team.blu)
        {
            teamBlu.SetActive(true);
            teamRed.SetActive(false);
        }
        else if (Team == Team.red)
        {
            teamBlu.SetActive(false);
            teamRed.SetActive(true);
        }
    }
    

    public override void MasterLogic()
    {
        buildingObserver.EnemyTeam = InvertedTeam;
        buildingObserver.EntityManager = entityManager;
        
        observer = buildingObserver;
        attack = buildingAttack;

        base.MasterLogic();
    }

    public override void RegularLogic()
    {
        RegisterCallBack((int)callbacks.GetHit, Callback_GetHit);
    }

    protected override void Tick()
    {
        var targets = observer.GetTargets(transform.position);
        if (targets != null && targets.Length > 0 && attack.CanAttack()) 
        {


            attack.Attack(targets[0]);
        }
    }

    protected override void OnDamageDelt(int amount)
    {
        healthBar.SetHealth(HP);

        base.OnDamageDelt(amount);
    }

    /// <summary>
    /// Callback na zasáhnutí.
    /// </summary>
    /// <param name="amount">Počet</param>
    void Callback_GetHit(object amount) 
    {
        var damage = (int)amount;
        DealDamage(damage);
    }
}
