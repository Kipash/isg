﻿using Model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : Building
{
    enum callbacks { none = 0, GetHit = 1, StateChanged = 2}

    [SerializeField] BuildingObserver buildingObserver;
    [SerializeField] BuildingAttack buildingAttack;

    [SerializeField] TowerEffects towerEffects;

    [SerializeField] HealthBar healthBar;

    public override float GetDPS()
    {
        if (attack.Delay == 0 || attack.Damage == 0)
            return 0;
        else
            return attack.Damage / attack.Delay;
    }

    public override float GetThreadLevel()
    {
        if (GetDPS() == 0f)     //If there is no DPS we need to assign some default value due to division by 0 error
            return 0.02f;
        else
            return Mathf.Sqrt(HP) / GetDPS();
    }

    //Mock
    Tile currentTile;


    public override void Initialize()
    {
        //print("Castle ini " + gameObject.name);

        if (!Initialized)
            towerEffects.Initialize();

        towerEffects.SetMaterials(Team);
        
        base.Initialize();

        healthBar.SetDefaultHealth(HP);
    }


    public override void MasterLogic()
    {
        buildingObserver.EnemyTeam = InvertedTeam;
        buildingObserver.EntityManager = entityManager;

        observer = buildingObserver;
        attack = buildingAttack;

        towerEffects.OnStateChanged = (state) =>
        {
            SendUpdate(new EntityUpdateData()
            {
                EntityID = ID,
                Payload = state,
                UpdateID = (int)callbacks.StateChanged
            });
        };

        
        buildingAttack.OnAttack = () =>
        {
            //print("OnAttack");
            towerEffects.Attack();
        };

        /*
        currentTile = mapManager.GetTile(transform.position);
        if(currentTile != null)
            currentTile.SetOccupancy(Depth, ID);
        */

        base.MasterLogic();
    }

    public override void RegularLogic()
    {
        RegisterCallBack((int)callbacks.StateChanged, towerEffects.ReceiveState);
        RegisterCallBack((int)callbacks.GetHit, Callback_GetHit);
    }

    protected override void Tick()
    {
        var targets = observer.GetTargets(transform.position);
        if (targets != null && targets.Length > 0 && attack.CanAttack())
            attack.Attack(targets[0]);
    }

    public override void Deactivate()
    {
        /*
        if(currentTile != null)
            currentTile.SetOccupancy(Depth, -1);
        */

        base.Deactivate();
    }

    protected override void OnDamageDelt(int amount) 
    {
        healthBar.SetHealth(HP);

        base.OnDamageDelt(amount);
    }

    /// <summary>
    /// Callback na zasáhnutí.
    /// </summary>
    /// <param name="amount">Počet</param>
    void Callback_GetHit(object amount) {
        var damage = (int)amount;
        DealDamage(damage);
    }
}