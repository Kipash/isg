﻿using System;
using System.Linq;
using UnityEngine;

[Serializable]
public class BuildingObserver : IEntityObeserver
{
    [SerializeField] float range;
    [SerializeField] Transform transform;
    [SerializeField] EntityClass[] classesToTarget;

    [HideInInspector] public EntityManager EntityManager;
    [HideInInspector] public Team EnemyTeam;

    public Entity[] GetTargets(Vector3 currentPosition)
    {
        var targets = EntityManager.Entities.Values
                        .Where(e => e.Team == EnemyTeam &&
                                    e.Targetable &&
                                    classesToTarget.Contains(e.Class) &&
                                    range >= Vector3.Distance(e.transform.position, transform.position))
                        .OrderBy(x => x.GetThreadLevel())
                        ;

        if (targets.Any())
            return targets.ToArray();
        else
            return null;
    }
}