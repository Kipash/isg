﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TowerEffects
{
    enum State { none = -1, Attack = 2 }

    [Header("Graphics")]
    [SerializeField] Animator animator;
    [SerializeField] Renderer[] teamRenderers;
    [SerializeField] Material teamBlu;
    [SerializeField] Material teamRed;

    [SerializeField] State[] states;
    [SerializeField] string[] animTriggers;

    State currentState;

    Dictionary<State, string> triggers = new Dictionary<State, string>();
    Dictionary<State, Action> processors = new Dictionary<State, Action>();

    public Action<object> OnStateChanged;

    public void Initialize()
    {
        for (int i = 0; i < states.Length; i++)
        {
            triggers.Add(states[i], animTriggers[i]);
        }

        processors.Add(State.Attack, () =>
        {
            SetTrigger(State.Attack);
        });
    }

    void Process(State s)
    {
        if (!processors.ContainsKey(s))
            return;

        processors[s].Invoke();

        currentState = s;
        if (OnStateChanged != null)
            OnStateChanged(s);
    }
    
    public void Attack() { Process(State.Attack); }

    void SetTrigger(State s)
    {
        animator.SetTrigger(triggers[s]);
    }

    public void SetMaterials(Team team)
    {
        var mat = team == Team.blu ? teamBlu : teamRed;
        foreach (var x in teamRenderers)
        {
            x.material = mat;
        }
    }

    public void ReceiveState(object s)
    {
        var state = (State)s;

        Process(state);
    }
}
