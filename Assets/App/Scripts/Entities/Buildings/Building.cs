﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Building : Entity
{
    protected IAttack attack;
    protected IEntityObeserver observer;
}
