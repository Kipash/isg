﻿using System;
using UnityEngine;

[Serializable]
public class BuildingAttack : IAttack
{
    [SerializeField] int damage;
    [SerializeField] float delay;
    
    public int Damage { get { return damage; } set { damage = value; } }

    public float Delay { get { return delay; } }

    public float MinDistance { get { return -1; } }

    public Action OnAttack;

    float attackTimestamp;

    public bool Attack(Entity target)
    {
        attackTimestamp = Time.time;
        var dead = target.DealDamage(damage);
        if (OnAttack != null) OnAttack();
        return dead;
    }

    public bool CanAttack()
    {
        return Time.time - attackTimestamp > delay;
    }
}