﻿using Model;
using PathFind;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovement
{
    bool IsMoving { get; set; }

    void RemoveOccupancy();
    IEnumerator Start(List<Tile> tiles);
    IEnumerator Start(List<Vector3> tiles);
}
