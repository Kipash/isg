﻿using UnityEngine;
using System;
using System.Collections;
using Model;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class StepMovement : IMovement
{
    public bool IsMoving { get; set; }
    Tile currentTile;

    [SerializeField] float speed;
    
    [HideInInspector] public int Depth;
    [HideInInspector] public MapHexManager MapManager;
    [HideInInspector] public Transform Transform;
    [HideInInspector] public int ID;


    public void RemoveOccupancy()
    {
        if (currentTile != null)
            currentTile.SetOccupancy(Depth, -1);
    }

    public IEnumerator Start(List<Tile> tiles)
    {
        if (tiles.Count  == 0)
            yield break;

        IsMoving = true;

        currentTile = tiles.First();
        
        //Kam
        var target = MapManager.GetWorldCoordinates(currentTile);
        if (target.IsDefault())
        {
            Debug.LogFormat(Log.Dye("{0} coudln't find tile", 1), Transform.gameObject.name);
            currentTile = null;
            yield break; //Pokud nebyl nalezený cíl
        }

        //Odkud
        var lastPos = Transform.position;
        //Směr
        var lookVector = target - lastPos;

        if (lookVector != Vector3.zero)
            Transform.rotation = Quaternion.LookRotation(lookVector, Vector3.up);

        var distance = Vector3.Distance(Transform.position, target);

        var initialTime = Time.time;
        var timeOfArrival = initialTime + (distance / speed);

        var ration = timeOfArrival - initialTime;

        var initialPosition = Transform.position;

        currentTile.SetOccupancy(Depth, ID);

        while (true)
        {
            //Debug
            Debug.DrawLine(Transform.position, target, Color.magenta);
            Debug.DrawLine(target, target + (Vector3.up / 8), Color.red);
            
            if (Time.time > timeOfArrival)
            {
                Transform.position = target;
                break;
            }

            var transitionInverted = Mathf.Clamp01((timeOfArrival - Time.time) / ration);
            var transition = Mathf.Abs(transitionInverted - 1);

            var newPos = Vector3.Lerp(initialPosition, target, transition);

            if (!float.IsNaN(newPos.x) && !float.IsNaN(newPos.y) && !float.IsNaN(newPos.z))
                Transform.position = newPos;

            yield return null;
        }

        currentTile.SetOccupancy(Depth, -1);
        IsMoving = false;
        yield return null;
    }

    public IEnumerator Start(List<Vector3> path)
    {
        IsMoving = true;

        var lastPos = Transform.position;

        for (int i = 0; i < path.Count; i++)
        {
            var target = path[i];

            var lookVector = target - lastPos;

            if (lookVector != Vector3.zero)
                Transform.rotation = Quaternion.LookRotation(lookVector, Vector3.up);

            var distance = Vector3.Distance(Transform.position, target);

            var initialTime = Time.time;
            var timeOfArrival = initialTime + (distance / speed);

            var ration = timeOfArrival - initialTime;

            var initialPosition = Transform.position;

            while (true)
            {
                if (Time.time > timeOfArrival)
                {
                    Transform.position = target;
                    break;
                }

                var transitionInverted = Mathf.Clamp01((timeOfArrival - Time.time) / ration);
                var transition = Mathf.Abs(transitionInverted - 1);

                var newPos = Vector3.Lerp(initialPosition, target, transition);

                if (!float.IsNaN(newPos.x) && !float.IsNaN(newPos.y) && !float.IsNaN(newPos.z))
                    Transform.position = newPos;

                yield return null;
            }

            lastPos = target;
        }

        IsMoving = false;
    }
}
