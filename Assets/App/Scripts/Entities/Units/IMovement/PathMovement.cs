﻿using UnityEngine;

using Model;
using PathFind;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class PathMovement : IMovement
{
    [SerializeField] Transform transform;
    [SerializeField] float speed;
    public MapHexManager MapManager { get; set; }
    public int ID { get; set; }

    public int Depth { get; set; }

    public int OriginIndex { get { return originIndex; } }
    int originIndex = -1;
    public int TargetIndex { get { return targetIndex; } }
    int targetIndex = -1;

    public bool IsMoving { get; set; }

    List<Tile> currentPath;

    public IEnumerator Start(List<Tile> path)
    {
        IsMoving = true;
        currentPath = path;

        var lastPos = transform.position;

        for (int i = 0; i < path.Count; i++)
        {
            targetIndex = i;
            originIndex = i - 1;

            var target = MapManager.GetWorldCoordinates(path[i]);
            if (target.IsDefault())
                continue;

            var lookVector = target - lastPos;

            if (lookVector != Vector3.zero)
                transform.rotation = Quaternion.LookRotation(lookVector, Vector3.up);

            var distance = Vector3.Distance(transform.position, target);

            var initialTime = Time.time;
            var timeOfArrival = initialTime + (distance / speed);

            var ration = timeOfArrival - initialTime;

            var initialPosition = transform.position;
            
            path[i].SetOccupancy(Depth, ID);
            
            while (true)
            {
                //Debug
                if (path.Count > 2)
                {
                    for (int pdIndex = 0; pdIndex < path.Count - 1; pdIndex++)
                    {
                        var p1 = MapManager.GetWorldCoordinates(path[pdIndex]);
                        var p2 = MapManager.GetWorldCoordinates(path[pdIndex + 1]);
                        Debug.DrawLine(p1, p2, Color.red);
                        Debug.DrawLine(p1, p1 + (Vector3.up * .2f), Color.magenta);
                        Debug.DrawLine(p2, p2 + (Vector3.up * .2f), Color.magenta);
                    }
                }
                
                if (Time.time > timeOfArrival)
                {
                    transform.position = target;
                    break;
                }

                var transitionInverted = Mathf.Clamp01((timeOfArrival - Time.time) / ration);
                var transition = Mathf.Abs(transitionInverted - 1);

                var newPos = Vector3.Lerp(initialPosition, target, transition);

                if (!float.IsNaN(newPos.x) && !float.IsNaN(newPos.y) && !float.IsNaN(newPos.z))
                    transform.position = newPos;

                yield return null;
            }
            
            path[i].SetOccupancy(Depth, -1);
            lastPos = target;
        }

        IsMoving = false;
        //OnStateChanged(this);
    }

    public IEnumerator Start(List<Vector3> path)
    {
        IsMoving = true;

        var lastPos = transform.position;
        
        for (int i = 0; i < path.Count; i++)
        {
            targetIndex = i;
            originIndex = i - 1;

            var target = path[i];

            var lookVector = target - lastPos;

            if(lookVector != Vector3.zero)
                transform.rotation = Quaternion.LookRotation(lookVector, Vector3.up);

            var distance = Vector3.Distance(transform.position, target);

            var initialTime = Time.time;
            var timeOfArrival = initialTime + (distance / speed);

            var ration = timeOfArrival - initialTime;

            var initialPosition = transform.position;
            
            while (true)
            {
                /*
                for (int o = 0; o < path.Count - 1; o++)
                {
                    var team = transform.GetComponent<Entity>().Team;
                    var offset = Vector3.up * (team == Team.blu ? 0.5f : 0.25f);
                    Debug.DrawLine(path[o] + offset, path[o + 1] + offset, team == Team.blu ? Color.cyan : Color.red);
                }
                */

                if (Time.time > timeOfArrival)
                {
                    transform.position = target;
                    break;
                }

                var transitionInverted = Mathf.Clamp01((timeOfArrival - Time.time) / ration);
                var transition = Mathf.Abs(transitionInverted - 1);

                var newPos = Vector3.Lerp(initialPosition, target, transition);

                if (!float.IsNaN(newPos.x) && !float.IsNaN(newPos.y) && !float.IsNaN(newPos.z))
                    transform.position = newPos;

                yield return null;
            }
            
            lastPos = target;
        }

        IsMoving = false;
    }

    public void RemoveOccupancy()
    {
        if (currentPath == null)
            return;

        var count = currentPath.Count;
        if (count > 0 && (targetIndex >= 0 && targetIndex < count))
        {
            Log.Write("RemoveOccupancy");
            currentPath[targetIndex].SetOccupancy(Depth, -1);
        }
    }
}
