﻿using Photon.Pun;
using UnityEngine;
using System.Linq;
using PathFind;
using Model;
using System.Collections.Generic;
using System;
using UnityEngine.AI;

/// <summary>
/// Základní implementace chování pozemních jednotek.
/// </summary>
public class Unit : Entity
{
    /// <summary>
    /// Lokální eventy.
    /// </summary>
    enum Callbacks { none = -1, SetPath = 0, GetHit = 1, StateChanged = 2, Position = 3, Buff = 4 }

    [Header("GroundUnit")]
    [SerializeField] GroundObserver observer;
    [SerializeField] GroundAttack   attack;

    public NavMeshAgent Agent;

    /// <summary>
    /// Effekty jako jsou animace apod.
    /// </summary>
    [SerializeField] UnitEffects unitEffects;

    [SerializeField] HealthBar healthBar;

    /// <summary>
    /// Inicializuje všechny interfaci, jako je třeba IAttack.
    /// </summary>
    public override void Initialize()
    {
        gameObject.SetActive(true);

        unitEffects.SetMaterials(Team);
        unitEffects.Idle();
        unitEffects.SetBuff(false);
        if (!Initialized)
            unitEffects.Initialize();
        
        base.Initialize();

        healthBar.SetDefaultHealth(HP);

        //TODO: change to server/client, this is p2p
        RegisterCallBack((int)Callbacks.Buff, (x) => { Buff(false); });
        
    }

    /// <summary>
    /// Nastaví patřičné části, tak aby fungovali pouze u mastera.
    /// </summary>
    public override void MasterLogic()
    {
        base.MasterLogic();
        
        observer.EntityManager = entityManager;
        observer.EnemyTeam = InvertedTeam;
        
        attack.OnAttack = () =>
        {
            unitEffects.Attack();
        };
        attack.Multiplier = 1f;

        unitEffects.OnStateChanged = (state) =>
        {
            SendUpdate(new EntityUpdateData()
            {
                EntityID = ID,
                Payload = state,
                UpdateID = (int)Callbacks.StateChanged
            });
        };
    }

    /// <summary>
    /// Nastavení callbacku pro eventy od mastera.
    /// </summary>
    public override void RegularLogic()
    {
        RegisterCallBack((int)Callbacks.SetPath, Callback_SetPath);
        RegisterCallBack((int)Callbacks.GetHit, Callback_GetHit);
        RegisterCallBack((int)Callbacks.StateChanged, unitEffects.ReceiveState);
        RegisterCallBack((int)Callbacks.Position, (x) => { transform.position = ((V3)x).Vector; StopMoving(false); });
    }
    
    /// <summary>
    /// Najde a vrátí entitu, kterou bude cílit.
    /// </summary>
    Entity Observe()
    {
        var targets = observer.GetTargets(transform.position);
        if (targets != null && targets.Length > 0)
            return targets[0];
        else
            return null;
    }

    /// <summary>
    /// Update entity, hledá cíl a cestu a potom zváží svojí strategii.
    /// </summary>
    protected override void Tick()
    {
        var target = Observe();
        if (target == null || target.transform == null)
        {
            Log.WriteF(Log.Dye("{0} ({1}) - can't find target or it's dead", 1), gameObject.name, Type);
            unitEffects.Idle();
            return;
        }
        EvaluateStrategy(target);
    }

    /// <summary>
    /// Zváží svojí strategii na základě parametrů
    /// </summary>
    /// <param name="target">Cíl</param>
    /// <param name="path">Cesta</param>
    void EvaluateStrategy(Entity target)
    {
        //Zaútočí pokud je dostatečně blízko
        if (attack.MinDistance > Vector3.Distance(transform.position, target.transform.position)) 
        {
            if (attack.CanAttack()) //firerate
            {
                StopMoving(true);
                attack.Attack(target);

                //Sents update about the attack to sync HP.
                SendUpdate(new EntityUpdateData()
                {
                    EntityID = target.ID,
                    Payload = attack.Damage,
                    UpdateID = (int)Callbacks.GetHit
                });
            }
        }
        // cíl je přílis daleko, musí se pohybovat.
        // zkontrolujeme zdali poslední cesta nevypada stejne a pokud ne, updatujeme IMovement s novím polem pozic.
        else
        {
            //Pošle update o rozhodnuté cestě ostatním klientům.
            SendUpdate(new EntityUpdateData()
            {
                EntityID = ID,
                Payload = new V3(target.transform.position),
                UpdateID = (int)Callbacks.SetPath
            });

            //Animace postavy
            unitEffects.Move();

            //Začne se hýbat.
            StartMoving(target.transform.position);
        }
    }

    /// <summary>
    /// Callback na nastevení cesty, dostane pozice a jde po nich.
    /// </summary>
    /// <param name="path">Cesta (Vector3[])</param>
    void Callback_SetPath(object path)
    {
        var pos = ((V3)path).Vector;
        StartMoving(pos);
    }

    /// <summary>
    /// Callback na zasáhnutí.
    /// </summary>
    /// <param name="amount">Počet</param>
    void Callback_GetHit(object amount)
    {
        var damage = (int)amount;
        DealDamage(damage);
    }

    /// <summary>
    /// Zastaví jednotku. Přestane s ní pohybovat
    /// </summary>
    /// <param name="sync"><c>true</c> je master, <c>false</c> = je regular.</param>
    void StopMoving(bool sync)
    {
        unitEffects.Idle();

        Agent.isStopped = true;
        if (!sync)
            return;
        SendUpdate(new EntityUpdateData()
        {
            EntityID = ID,
            Payload = new V3(transform.position),
            UpdateID = (int)Callbacks.Position
        });
    }
    void StartMoving(Vector3 target)
    {
        Agent.isStopped = false;
        Agent.SetDestination(target);
    }

    /// <summary>
    /// Připraveno pro healthbar. 
    /// </summary>
    /// <param name="amount">Amount.</param>
    protected override void OnDamageDelt(int amount)
    {
        healthBar.SetHealth(HP);

        base.OnDamageDelt(amount);
    }

    /// <summary>
    /// Nastaví bonusi jednotce.
    /// </summary>
    public void Buff(bool sync = true)
    {
        //TODO: change to client/server, this is p2p.
        if (sync)
        {
            SendUpdate(new EntityUpdateData()
            {
                EntityID = ID,
                Payload = 0,
                UpdateID = (int)Callbacks.Buff
            });
        }

        HP *= 2;
        attack.Multiplier = 1.5f;
        unitEffects.SetBuff(true);
    }

    public override float GetDPS()
    {
        return 1;
    }

    public override float GetThreadLevel()
    {
        return 1;
    }
}