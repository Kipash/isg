﻿using UnityEngine;

using System;
using System.Collections;
using System.Linq;

using Model;
using PathFind;

[Serializable]
public class GroundObserver : IEntityObeserver
{
    [HideInInspector] public EntityManager EntityManager;
    [HideInInspector] public Team EnemyTeam;
    [SerializeField] float range = 4;
    public float Range { get { return range * MapHexManager.Spacing; } }

    
    [SerializeField] EntityClass[] classesToTarget;

    public Entity[] GetTargets(Vector3 currentPosition)
    {
        var entities = EntityManager.Entities.Values;

        var enemyCastle = entities.FirstOrDefault(x =>
                                        x.Team == EnemyTeam
                                        && x.Type == EntityType.Castle);

        var targets = entities.Where(e =>
                                     classesToTarget.Contains(e.Class) &&
                                     e.Targetable &&
                                     e.Team == EnemyTeam &&
                                     Range >= Vector3.Distance(currentPosition, e.transform.position))
                               .OrderBy(x => x.GetThreadLevel());

        if (targets.Count() > 0)
            return targets.ToArray();
        else if (enemyCastle != null)
            return new Entity[] { enemyCastle };
        else
            return new Entity[] { };
    }
}
