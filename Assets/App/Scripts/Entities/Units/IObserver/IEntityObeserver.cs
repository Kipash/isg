﻿using UnityEngine;

public interface IEntityObeserver
{
    Entity[] GetTargets(Vector3 currentPosition);
}