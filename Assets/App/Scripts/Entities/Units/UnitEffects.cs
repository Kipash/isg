﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class UnitEffects
{
    [Serializable]
    class TriggerData
    {
        public State State;
        public string Trigger;
    }

    enum State { none = -1, Idle = 0, Move = 1, Attack = 2 }

    [Header("Graphics")]
    [SerializeField] Animator animator;
    [SerializeField] Renderer[] teamRenderers;
    [SerializeField] Material teamBlu;
    [SerializeField] Material teamRed;

    [SerializeField] TriggerData[] triggerData;

    [SerializeField] GameObject buffIndicator;

    State currentState;

    Dictionary<State, string> triggers = new Dictionary<State, string>();
    Dictionary<State, Action> processors = new Dictionary<State, Action>();

    public Action<object> OnStateChanged;


    public void Initialize()
    {
        foreach(var x in triggerData)
        {
            triggers.Add(x.State, x.Trigger);
        }

        processors.Add(State.Idle, () =>
        {
            if (currentState == State.Idle)
                return;

            SetTrigger(State.Idle);
        });
        processors.Add(State.Move, () =>
        {
            if (currentState == State.Move)
                return;

            SetTrigger(State.Move);
        });
        processors.Add(State.Attack, () =>
        {
            SetTrigger(State.Attack);
        });
    }

    void Process(State s)
    {
        if (!processors.ContainsKey(s) || currentState == s)
            return;
        
        processors[s].Invoke();

        currentState = s;
        if (OnStateChanged != null)
            OnStateChanged(s);
    }

    public void Idle()   { Process(State.Idle);   }
    public void Move()   { Process(State.Move);   }
    public void Attack() { Process(State.Attack); }

    void SetTrigger(State s)
    {
        
        foreach(var x in triggers.Values)
        {
            animator.ResetTrigger(x);
        }

        animator.SetTrigger(triggers[s]);
    }

    public void SetMaterials(Team team)
    {
        var mat = team == Team.blu ? teamBlu : teamRed;
        foreach(var x in teamRenderers)
        {
            x.material = mat;
        }
    }

    public void ReceiveState(object s)
    {
        var state =(State)s;

        Process(state);
    }

    public void SetBuff(bool state)
    {
        buffIndicator.SetActive(state);
    }    
}
