﻿using Model;
using PathFind;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPathfinder
{
    Action<Path<Tile>> OnPathFound { get; set; } 
    void GetPath(Vector3 currentPosition, Vector3 targetPosition);
}
