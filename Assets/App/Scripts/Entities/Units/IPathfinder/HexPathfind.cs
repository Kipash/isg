﻿using UnityEngine;
using System;
using System.Collections;
using Model;
using PathFind;

/*
[Serializable]
public class HexPathfind : IPathfinder
{
    [HideInInspector] public Agent MapManager;
    [HideInInspector] public int ID = -1;
    
    public int Depth { get; set; }
    
    public Action<Path<Tile>> OnPathFound { get; set; }

    public void GetPath(Vector3 currentPosition, Vector3 targetPosition)
    {
        Action<Path<Tile>> callback = (x) =>
        {
            if (x == null)
                Log.Write("Path not found - " + currentPosition + " >>> " + targetPosition, 5);

            if(OnPathFound != null)
                OnPathFound(x);
        };

        MapManager.GetPathAsync(currentPosition, targetPosition, Depth, ID, callback);
    }
}
*/