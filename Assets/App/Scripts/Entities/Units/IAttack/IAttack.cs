﻿/// <summary>
/// Iterface pro chování útoku.
/// </summary>
public interface IAttack
{
    /// <summary>
    /// Poškození, které údá.
    /// </summary>
    int Damage { get; }
    /// <summary>
    /// Doba mezi ranamy.
    /// </summary>
    float Delay { get; }
    /// <summary>
    /// Minimalní vzdálenost pro útok.
    /// </summary>
    float MinDistance { get; }

    /// <summary>
    /// Může útočit?
    /// </summary>
    bool CanAttack();

    /// <summary>
    /// Zaútočit na entitu.
    /// </summary>
    /// <param name="target">Entita na kterou se útočí.</param>
    /// <returns><c>True</c> - zemřela, <c>False</c> - žije.</returns>
    bool Attack(Entity target);
}