﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Implementace IAttack.
/// </summary>
[Serializable]
public class GroundAttack : IAttack
{
    /// <summary>
    /// Event jakmile se zaútočení.
    /// </summary>
    public Action OnAttack;

    [SerializeField] int damage;
    [SerializeField] float delay;
    [SerializeField] float minDistance;
    float attackTimestamp;
    [HideInInspector] public float Multiplier = 1;
    public int Damage
    {
        get
        {
            return (int)(damage * Multiplier);
        }
    }

    public float Delay
    {
        get
        {
            return delay;
        }
    }

    public float MinDistance
    {
        get
        {
            return minDistance * MapHexManager.Spacing;
        }
    }

    /// <summary>
    /// Returns if the entity died.
    /// </summary>
    /// <param name="target">Targeted entity</param>
    /// <param name="damage">Amount</param>
    /// <returns></returns>
    public bool Attack(Entity target)
    {
        attackTimestamp = Time.time;

        if (OnAttack != null)
            OnAttack();

        return target.DealDamage(Damage);
    }

    /// <summary>
    /// Zkontroluje zdali neporušuje firerate.
    /// </summary>
    public bool CanAttack()
    {
        return Time.time - attackTimestamp > delay;
    }
}
