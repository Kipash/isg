﻿using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine;

public class PlayerData
{
    public Player Player;
    public string Name { get { return Player.NickName; } }

    //CustomData
}
