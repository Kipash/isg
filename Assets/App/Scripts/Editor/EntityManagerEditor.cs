﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GameServices))]
[CanEditMultipleObjects]
public class EntityManagerEditor : Editor {

    public override void OnInspectorGUI()
    {
        var gs = (GameServices)target;

        DrawDefaultInspector();

        if(gs.EntityManager.Entities != null)
        {
            var count = gs.EntityManager.Entities.Keys.Count;
            EditorGUILayout.HelpBox("Entities count: " + count, MessageType.None, true);
        }
    }
}
