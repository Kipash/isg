﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Unit), true)]
public class UnitEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var u = (Unit)target;

        DrawDefaultInspector();

        var doesTick = u.IsInvoking(Entity.TickFunctionName);
        string entity = string.Format("<Entity>\nActive: {0}\nTeam: {1}\nTick: {2}\nHP: {3}",
                                u.Active ? "yes" : "no", u.Team, doesTick ? "yes" : "no", u.HP);
        
        EditorGUILayout.HelpBox(entity, MessageType.None, true);
    }
}