﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Realtime;
using UnityEngine;

public class RoomNetworkCallBacks : IInRoomCallbacks
{
    public Action<Player> _OnMasterClientSwitched;
    public Action<Player> _OnPlayerEnteredRoom;
    public Action<Player> _OnPlayerLeftRoom;
    public Action<Player, ExitGames.Client.Photon.Hashtable> _OnPlayerPropertiesUpdate;
    public Action<ExitGames.Client.Photon.Hashtable> _OnRoomPropertiesUpdate;

    public void OnMasterClientSwitched(Player newMasterClient)
    {
        if(_OnMasterClientSwitched != null)
            _OnMasterClientSwitched.Invoke(newMasterClient);
    }

    public void OnPlayerEnteredRoom(Player newPlayer)
    {
        if (_OnPlayerEnteredRoom != null)
            _OnPlayerEnteredRoom.Invoke(newPlayer);
    }

    public void OnPlayerLeftRoom(Player otherPlayer)
    {
        if (_OnPlayerLeftRoom != null)
            _OnPlayerLeftRoom.Invoke(otherPlayer);

        //Temp
    #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
    #endif
        Application.Quit();
    }

    public void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        if (_OnPlayerPropertiesUpdate != null)
            _OnPlayerPropertiesUpdate.Invoke(targetPlayer, changedProps);
    }

    public void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        if (_OnRoomPropertiesUpdate != null)
            _OnRoomPropertiesUpdate.Invoke(propertiesThatChanged);
    }
}
