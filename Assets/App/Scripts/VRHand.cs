﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

/// <summary>
/// Efekty a zpráva jednotlivého ovladače ve VR.
/// </summary>
public class VRHand : MonoBehaviour
{
    [SerializeField] Color blueCasts;
    [SerializeField] Color redCasts;

    [SerializeField] GameObject marker;

    [SerializeField] TrailRenderer trail;
    [SerializeField] ParticleSystem spellParticles;

    [SerializeField] Transform throwableParent;

    public Vector3 Position { get { return transform.position; } }

    public Transform Marker { get { return marker.transform; } }

    public SteamVR_Input_Sources Source { get; set; }

    void Awake()
    {
        if (spellParticles != null && trail != null)
        {
            var main = spellParticles.main;
            main.startColor = PhotonNetwork.IsMasterClient ? blueCasts : redCasts;


            trail.startColor = PhotonNetwork.IsMasterClient ? blueCasts : redCasts;
            trail.endColor = PhotonNetwork.IsMasterClient ? blueCasts : redCasts;

            SetTrail(false);
        }
    }

    Vector3 lastPos;
    Vector3 direction;
    void FixedUpdate()
    {
        direction = -(lastPos - Position);
        lastPos = Position;
    }

    public void CastEffect()
    {
        spellParticles.Play();
    }

    public void SetTrail(bool state)
    {
        trail.emitting = state;
    }

    public void SetMarker(bool state)
    {
        marker.SetActive(state);
    }

    public void SetMarker(Vector3 point)
    {
        marker.transform.position = point;
    }

    ThrowableSpectre currentProjectile;

    public void InitializeThrow(EntityType projectileType)
    {
        GameServices.Inst.EntityManager.CreateEntity(new EntityData()
        {
            InitialPosition = new V3(Vector3.zero),
            InitialRotation = new Q(Quaternion.identity),
            Team = GameManager.PlayerTeam,
            Type = projectileType,
        },
        (entity) =>
        {
            currentProjectile = entity as ThrowableSpectre;

            if (currentProjectile == null)
            {
                AppServices.Inst.EntityPool.DeactivateEntity(entity);
                return;
            }

            var hand = -1;
            if (Source == SteamVR_Input_Sources.LeftHand)
                hand = 0;
            else if (Source == SteamVR_Input_Sources.RightHand)
                hand = 1;
            else //incase the source is different
                return;

            currentProjectile.Mount(hand);
        });
    }

    public void DetatchProjectile()
    {
        var dir = direction.magnitude < 0.03f ? throwableParent.forward : direction;
        currentProjectile.Launch(dir);
    }

    public void Mount(Transform t)
    {
        t.SetParent(throwableParent);
        t.localPosition = Vector3.zero;
        t.localRotation = Quaternion.identity;
        t.localScale = Vector3.one;
    }
}
