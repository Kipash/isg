﻿using UnityEngine;
using System.Collections;
using Photon.Realtime;
using System.Collections.Generic;
using System;

public class GlobalNetworkCallBacks : IConnectionCallbacks
{
    public Action _OnConnected;
    public Action _OnConnectedToMaster;
    public Action<DisconnectCause> _OnDisconnected;
    public Action<RegionHandler> _OnRegionListReceived;
    public Action<Dictionary<string, object>> _OnCustomAuthenticationResponse;
    public Action<string> _OnCustomAuthenticationFailed;

    public void OnConnected()
    {
        if(_OnConnected != null)
            _OnConnected.Invoke();
    }

    public void OnConnectedToMaster()
    {
        if(_OnConnectedToMaster != null)
            _OnConnectedToMaster.Invoke();
    }

    public void OnDisconnected(DisconnectCause cause)
    {
        if (_OnDisconnected != null)
            _OnDisconnected.Invoke(cause);
    }

    public void OnRegionListReceived(RegionHandler regionHandler)
    {
        if (_OnRegionListReceived != null)
            _OnRegionListReceived.Invoke(regionHandler);
    }

    public void OnCustomAuthenticationResponse(Dictionary<string, object> data)
    {
        if (_OnCustomAuthenticationResponse != null)
            _OnCustomAuthenticationResponse.Invoke(data);
    }

    public void OnCustomAuthenticationFailed(string debugMessage)
    {
        if (_OnCustomAuthenticationFailed != null)
            _OnCustomAuthenticationFailed.Invoke(debugMessage);
    }
}
