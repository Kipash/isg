﻿using UnityEngine;
using System.Collections;
using System;

//Used by Entity.cs and Prop.cs

[Serializable]
public class EntityUpdateData
{
    public int EntityID;
    public int UpdateID;
    public object Payload;
}