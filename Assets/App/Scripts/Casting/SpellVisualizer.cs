﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(LineRenderer))]
[ExecuteInEditMode]
public class SpellVisualizer : MonoBehaviour
{
    [SerializeField] LineRenderer lr;
    [SerializeField] int manualIndex;
    
    Vector3 center;

    /// <summary>
    /// Loads up all spells to have them ready for visualisation
    /// </summary>
    [ContextMenu("Reinitialize")]
    public void Start() {
        lr = GetComponent<LineRenderer>();
    }

    /// <summary>
    /// Updates the LineRenderer.
    /// </summary>
    public void DisplaySpell(Spell s) {
        if (lr) {
            center = Vector3.zero;

            var pointSet = s.points[manualIndex ].points;
            lr.positionCount = pointSet.Count;
            lr.SetPositions(pointSet.ToArray());
           
            //centering all spells
            for (int i = 0; i < lr.positionCount; i++){
                center += lr.GetPosition(i);
            }
            center /= lr.positionCount;
            
            for (int i = 0; i < lr.positionCount; i++) {
                 lr.SetPosition(i, lr.GetPosition(i) - center);
            }
        }
    }




}
