﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewSpell", menuName = "SpellCaster/Spell", order = 0)]
public class Spell : ScriptableObject {

    public List<PointSet> points = new List<PointSet>();
    public string id;

    public Spell() {
        points = new List<PointSet>();
    }

    public void Circulize(int stepps) {

        float angle = 360f / ((float)stepps);

        for (int i = 1; i < stepps; i++){
            List<Vector3> n = new List<Vector3>();
            PointSet ps = new PointSet();
            ps.points = n;
            for (int s = 0; s < points[i-1].points.Count; s++) {
                n.Add(points[i - 1].points[s]);
            }
            points.Add(ps);
            for (int s = 0; s < points[i].points.Count; s++) {
                points[i].points[s] = rotatePointAroundAxis(points[i].points[s], angle, Vector3.up);
            }

        }

    }
    Vector3 rotatePointAroundAxis(Vector3 point, float angle, Vector3 axis) {
        Quaternion q = Quaternion.AngleAxis(angle, axis);
        return q * point; 
    }

}


[System.Serializable]
public class PointSet
{
    public List<Vector3> points;
    public PointSet()
    {
        points = new List<Vector3>();
    }

}

