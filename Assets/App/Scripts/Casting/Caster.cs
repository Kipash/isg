﻿using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

[Serializable]
public class Caster
{
    public Action<Spell, object> OnSpellCasted;

    [SerializeField] float trashhold;

    /// <summary>
    /// total spell point count
    /// </summary>
    [SerializeField] int spellPointCount = 80;

    /// <summary>
    /// last casting points position
    /// </summary>
    private Vector3 lastPos;

    /// <summary>
    /// Distance between spell points
    /// </summary>
    [SerializeField] float dist = 0.05f;

    /// <summary>
    /// spell point position origin
    /// </summary>
    private Vector3 beggining;
    
    /// <summary>
    /// recorded points
    /// </summary>
    private List<Vector3> points = new List<Vector3>();

    /// <summary>
    /// list of known spells loaded from assets
    /// </summary>
    [SerializeField] List<Spell> spellbook = new List<Spell>();

    /// <summary>
    /// How many rotation instances to consider
    /// </summary>
    [SerializeField] int rotationSegments = 10;

    /// <summary>
    /// If the spell reference isn't as long as spellPointCount variable, stretch the point array to this length
    /// Todo: better stretching, this is horrible
    /// </summary>
    List<Vector3> Stretch(List<Vector3> lin)
    {
        List<Vector3> t = new List<Vector3>();
        for (int k = 0; k < lin.Count; k++)
        {
            t.Add(lin[k]);
        }
        int i = 0;
        while (t.Count < spellPointCount)
        {
            t.Insert(i + 1, Vector3.Lerp(t[i], t[i + 1], 0.5f));
            i += 2;
            if (i >= t.Count - 2)
                i = 0;
        }
        return t;
    }

    public void Initialize()
    {
        var spells = Resources.LoadAll<Spell>("Spells");
        spellbook.AddRange(spells);

        /*
        //load already known spells to spellbook
        string[] folders = new string[1];
        folders[0] = "Assets/Data/Spells";
        string[] sp = AssetDatabase.FindAssets("", folders);
        foreach (var item in sp)
        {
            string p = AssetDatabase.GUIDToAssetPath(item);
            spellbook.Add(AssetDatabase.LoadAssetAtPath<Spell>(p));
        }
        */
    }

    /// <summary>
    /// GetStateDown Of players casting
    /// </summary>
    public void BeginCast(Vector3 castPosition)
    {
        lastPos = castPosition;
        beggining = lastPos;

        points = new List<Vector3>();
        points.Add(Vector3.zero);
        //Instantiate trail here
    }

    /// <summary>
    /// GetState  Of players casting
    /// </summary>
    public void ContinueCast(Vector3 castPosition)
    {
        if (Vector3.Distance(castPosition, lastPos) > dist)
        {
            lastPos = castPosition;
            //Instantiate trail here
            points.Add((lastPos - beggining));
        }
    }

    /// <summary>
    /// GetStateUp  Of players casting
    /// </summary>
    public void EndCast(Vector3 castPosition, object context)
    {
        if (points.Count <= 3)
            return;

        Spell tmp = ScriptableObject.CreateInstance<Spell>();
        PointSet p = new PointSet();

        p.points = Stretch(points);
        tmp.points.Add(p);

        int index = -1;
        float distance = -1f;

        CheckSpellCast(tmp, spellbook, out index, out distance);

        if (distance <= trashhold)
        {
            var a = Mathf.Abs(distance - trashhold) / trashhold;
            var b = a * 100;

            if (OnSpellCasted != null)
                OnSpellCasted(spellbook[index], context);

            Debug.LogFormat("> {0} - {1}%",spellbook[index].name, (int)b);
        }
        else if(distance > float.MaxValue - 100f)
        {
            Debug.Log("No spells");
        }
        else
        {
            Debug.LogFormat("Unrecognized - raw distance: {0}", distance);
        }

    }

    void CheckSpellCast(Spell spell, List<Spell> spellBook, out int index, out float distance)
    {
        float smallestDistance = float.MaxValue;
        int bestIndex = -1;

        for (int i = 0; i < spellBook.Count; i++)
        {
            foreach (var pointSet in spellBook[i].points)
            {
                //0 in the index here because there is no need to rotate both compared spells, only one of them
                float result = ComparePointSets(spell.points[0], pointSet, smallestDistance);
                if (result < smallestDistance)
                {
                    smallestDistance = result;
                    bestIndex = i;
                }
            }
            ///TODO: DISCARD A RESULT IF IT'S TOO FAR FOR THIS PARTICULAR SPELL
        }

        //variable index now holds id of which spell was casted from your spellbook
        //variable dist now holds how far was it

        index = bestIndex;
        distance = smallestDistance;
    }

    /// <summary>
    /// GetStateDown of player recording a spell
    /// </summary>
    /// <param name="castPosition">Point of where it's casting.</param>
    public void BeginRecording(Vector3 castPosition)
    {
        lastPos = castPosition;
        beggining = lastPos;

        //instantiate particle here
        points = new List<Vector3>();
        points.Add(Vector3.zero);
    }

    /// <summary>
    /// GetState of player recording a spell
    /// </summary>
    /// <param name="castPosition">Point of where it's casting.</param>
    public void ContinueRecording(Vector3 castPosition)
    {
        if (Vector3.Distance(castPosition, lastPos) > dist)
        {
            lastPos = castPosition;
            //instantiate a particle
            points.Add(lastPos - beggining);
        }
    }

    static int spellID = 1;

    /// <summary>
    /// GetStateDown of player recording a spell
    /// </summary>
    /// <param name="castPosition">Point of where it's casting.</param>
    public void EndRecording(Vector3 castPosition)
    {
        string id = (spellID++).ToString();
        //string id = Random.Range(0, 100000).ToString(); //SET YOUR NUM ID FOR THIS SPELL HERE
        Spell s = ScriptableObject.CreateInstance<Spell>();

        s.id = id;
        points = Stretch(points);
        PointSet p = new PointSet();
        p.points = points;

        s.points.Add(p);
        s.Circulize(rotationSegments);

        //save spell to assets
#if UNITY_EDITOR
        AssetDatabase.CreateAsset(s, "Assets/Data/Resources/Spells/" + s.id + ".asset");
        AssetDatabase.Refresh();
        EditorUtility.SetDirty(s);
        AssetDatabase.SaveAssets();
#endif

        //add to known spellbook
        spellbook.Add(s);
    }

    /// <summary>
    /// Returns the total float distance between two PointSets with offsets.
    /// </summary>
    /// <param name="s1">PointSet 1</param>
    /// <param name="s2">PointSet 2</param>
    /// <param name="f1">offset beginning of the first PointSet (starting index)</param>
    /// <param name="f2">offset beginning of the second PointSet (starting index)</param>
    /// <param name="bDist">Best distance so far. If this is already better, no point in continuing.</param>
    /// <returns></returns>
    public static float GetDist(PointSet s1, PointSet s2, int f1, int f2, float bDist)
    {

        //overflow prevention, only compare till the shortest array ends
        int limit = s1.points.Count < s2.points.Count ? s1.points.Count : s2.points.Count;
        float totalDist = 0f;

        for (int l = f1, r = f2; l < limit && r < limit; l++, r++)
        {
            totalDist += Vector3.Distance(s1.points[l], s2.points[r]);
            if (totalDist > bDist)
                break;
        }

        return totalDist;
    }

    /// <summary>
    /// Returns the total float distance between two PointSets.
    /// </summary>
    /// <param name="s1">PointSet 1</param>
    /// <param name="s2">PointSet 2</param>
    /// <param name="bDist">Best distance so far. If this is already better, no point in continuing.</param>
    /// <returns></returns>
    public static float ComparePointSets(PointSet s1, PointSet s2, float bDist)
    {
        int limit = s1.points.Count < s2.points.Count ? s1.points.Count : s2.points.Count;
        float totalDist = float.MaxValue;
        int offsetCount = 10; // how many first values of each point set can be ignored in the calculation

        for (int l = 0; l < offsetCount; l++)
        {

            for (int r = 0; r < offsetCount; r++)
            {
                float cDistance = GetDist(s1, s2, l, r, bDist);
                if (cDistance > bDist)
                    break;

                totalDist = cDistance < totalDist ? cDistance : totalDist;

            }
        }

        return totalDist;
    }

    /*

    bool GetButton(SteamVR_Action_Boolean action, SteamVR_Input_Sources source)
    {
        return action.GetState(source);
    }
    bool GetButtonDown(SteamVR_Action_Boolean action, SteamVR_Input_Sources source)
    {
        return action.GetStateDown(source);
    }
    bool GetButtonUp(SteamVR_Action_Boolean action, SteamVR_Input_Sources source)
    {
        return action.GetStateUp(source);
    }

    void CheckSource(SteamVR_Action_Boolean action, SteamVR_Input_Sources source,
                     Action onState, Action onStateUp, Action onStateDown)
    {
        if (GetButtonDown(action, source))
        {
            onStateDown();
        }
        else if (GetButton(action, source))
        {
            onState();
        }
        else if (GetButtonUp(action, source))
        {
            onStateUp();
        }
    }
    
    void CheckSpell(SteamVR_Action_Boolean action, SteamVR_Input_Sources source, CastingSpot castingSpot)
    {
        CheckSource(action, source, () => { castingSpot.SetTrail(true ); ContinueCast(castingSpot.Position); },
                                    () => { castingSpot.SetTrail(false); EndCast(castingSpot.Position); },
                                    () => { castingSpot.SetTrail(true ); BeginCast(castingSpot.Position); });
    }
    void CheckRecord(SteamVR_Action_Boolean action, SteamVR_Input_Sources source, CastingSpot castingSpot)
    {
        CheckSource(action, source, () => { castingSpot.SetTrail(true ); ContinueRecording(castingSpot.Position); },
                                    () => { castingSpot.SetTrail(false); EndRecording(castingSpot.Position); },
                                    () => { castingSpot.SetTrail(true ); BeginRecording(castingSpot.Position); });
    }

    public void CheckForInputs(SteamVR_Action_Boolean cast, SteamVR_Action_Boolean record)
    {
        var leftHand =  SteamVR_Input_Sources.LeftHand;
        var rightHand = SteamVR_Input_Sources.RightHand;

        CheckSpell(cast, leftHand, leftCastSpot);
        CheckSpell(cast, rightHand, rightCastSpot);

        CheckRecord(record, leftHand, leftCastSpot);
        CheckRecord(record, rightHand, rightCastSpot);
    }
    */
}