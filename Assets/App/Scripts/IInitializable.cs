﻿public interface IInitializable
{
    void Initialize(object instance);
}