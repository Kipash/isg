﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DevLog : MonoBehaviour, ILog
{
    const int MaxMsgLength = 300;
    const int MaxBufferLines = 15;

    string buffer;

    [SerializeField] Text log;
    public void LogMessage(string msg)
    {
        if (msg.Length > MaxMsgLength)
            return;
        
        buffer += msg + "\n";

        var split = buffer.Split(new string[]{ "\n" }, System.StringSplitOptions.None);

        if (split.Length > MaxBufferLines)
        {
            var l = split.ToList();
            l.RemoveAt(0);

            buffer = l.Aggregate((x,y) => x + "\n" + y);
        }

        log.text = buffer;
    }
}
