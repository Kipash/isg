﻿using UnityEngine;
using System.Collections;

public class UnityLog : ILog
{
    public void LogMessage(string msg)
    {
        msg = Log.Dye("<b>[S301]</b> ", 1) + msg;
        Debug.Log(msg);
    }
}
