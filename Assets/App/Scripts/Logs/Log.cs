﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Centralizované vypisování do různých výstupu, Unity log (Editor + Build log), on screen UI apod.
/// </summary>
public class Log
{
    static List<ILog> loggers = new List<ILog>();

    static Dictionary<int, Color> indexColors;

    /// <summary>
    /// Lazyloading předem definovaných barev podle čísla.
    /// </summary>
    /// <param name="index">
    /// 0 - Black
    /// 1 - Red
    /// 2 - Blue
    /// 3 - Cyan
    /// 4 - Green
    /// 5 - Magenta
    /// 6 - Yellow
    /// </param>
    static Color LoadOrCreateColor(int index)
    {
        if(indexColors == null)
        {
            indexColors = new Dictionary<int, Color>();
            indexColors.Add(0, Color.black);
            indexColors.Add(1, new Color(0.7f, 0.3f, 0.2f)); //red
            indexColors.Add(2, new Color(0.3f, 0.4f, 0.8f)); //blue
            indexColors.Add(3, new Color(0.2f, 0.7f, 0.7f)); //cyan
            indexColors.Add(4, new Color(0.2f, 0.6f, 0.2f)); //green
            indexColors.Add(5, new Color(0.8f, 0.3f, 0.9f)); //magenta
            indexColors.Add(6, new Color(0.6f, 0.6f, 0.2f)); //yellow

            return LoadOrCreateColor(index);
        }
        else
        {
            return indexColors.ContainsKey(index) ? indexColors[index] : Color.black;
        }
    }
    /// <summary>
    /// Byte 0 - 255 to hexidecimal string representation.
    /// </summary>
    static string ToHex(float c)
    {
        int n = (int)(c * 255);
        string hex = n.ToString("x2");
        return hex;
    }

    public static void Write(string s)
    {
        foreach (var x in loggers.Distinct())
        {
            x.LogMessage(s);
        }
    }
    public static void Write(string s, Color c)
    {
        Write(Dye(s, c));
    }
    public static void Write(string s, int c)
    {
        Write(Dye(s, LoadOrCreateColor(c)));
    }
    public static void WriteF(string s, int c, params object[] prms)
    {
        Write(Dye(string.Format(s, prms), LoadOrCreateColor(c)));
    }

    public static void WriteF(string s, params object[] prms)
    {
        Write(string.Format(s, prms));
    }

    public static string Dye(string s, Color c)
    {
        return "<color=#" + ToHex(c.r) + ToHex(c.g) + ToHex(c.b) + "ff>" + s + "</color>";
    }
    public static string Dye(string s, int c)
    {
        return Dye(s, LoadOrCreateColor(c));
    }

    public static void AddListener(ILog log)
    {
        loggers.Add(log);
    }
}
