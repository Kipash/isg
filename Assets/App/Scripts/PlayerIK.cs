﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIK : MonoBehaviour {

    //public Transform headset;
    //public Transform leftController;
    //public Transform rightController;

    [Header("Team")]
    [SerializeField] Renderer headRenderer;
    [SerializeField] bool invert;
    [SerializeField] Material blu;
    [SerializeField] Material red;

    [Header("IK")]

    [SerializeField] Transform head;
    [SerializeField] Transform left;
    [SerializeField] Transform right;
    [SerializeField] Transform body;
    [SerializeField] Transform lShoulder;
    [SerializeField] Transform rShoulder;

    [SerializeField] Transform lElbow;
    [SerializeField] Transform rElbow;

    [SerializeField] float verticalBodyOffset;
    [SerializeField] float frontBodyOffset;
    [SerializeField] float handOffset = 1f; // before -1


    private void Awake()
    {
        //ffs
        if(PhotonNetwork.IsMasterClient)
        {
            if (invert)
                headRenderer.material = red;
            else
                headRenderer.material = blu;
        }
        else
        {
            if (invert)
                headRenderer.material = blu;
            else
                headRenderer.material = red;
        }
    }

    public void ProcessIK(Vector3 headsetPos, Quaternion headsetRot,
                          Vector3 leftControllerPos, Quaternion leftControllerRot,
                          Vector3 rightControllerPos, Quaternion rightControllerRot)
    {
        head.position = headsetPos;
        head.localRotation = headsetRot;

        body.localRotation = Quaternion.AngleAxis(head.localEulerAngles.y, Vector3.up);
        body.position = headsetPos + (Vector3.up * verticalBodyOffset) + (body.forward * frontBodyOffset);
        
        ////------------------

        var leftHand = leftControllerPos;
        leftHand.y = leftControllerPos.y;
        leftHand.x = leftControllerPos.x;
        leftHand.z += handOffset;
        left.position = leftHand;
        
        var rightHand = rightControllerPos;
        rightHand.y = rightControllerPos.y;
        rightHand.x = rightControllerPos.x;
        rightHand.z += handOffset;
        right.position = rightHand;
        
        left.localRotation = leftControllerRot;
        right.localRotation = rightControllerRot;

    
        lShoulder.LookAt(lElbow);
        rShoulder.LookAt(rElbow);
    }
}
