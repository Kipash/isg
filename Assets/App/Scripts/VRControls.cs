﻿using Photon.Pun;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

/// <summary>
/// Simple action that player executes.
/// </summary>
public enum PlayerAction
{
    none = -1,
    SpawnKnight = 1,
    SpawnPikeman = 2,
    SpawnArcher = 3,
    SpawnCatapult = 4,

    SpawnTower = 50,

    PowerBuff = 100,

    Spear = 150,
    Bolt = 151,
}

/// <summary>
/// VR implementation of casting, placing spells and recording new spells.
/// </summary>
[Serializable]
public class VRControls : IControls, IUpdateable
{
    [Header("- VR - ")]
    [SerializeField] GameObject VRRig;
    public Transform Neck;

    [Header("- SteamVR - ")]
    [SerializeField] SteamVR_ActionSet actionSet;
    [Space(10)]
    [Header("   > Input")]
    [SerializeField] SteamVR_Action_Boolean trigger;
    [SerializeField] SteamVR_Action_Boolean grip;
    [SerializeField] SteamVR_Action_Boolean dpad;
    [SerializeField] SteamVR_Action_Boolean menu;
    [Header("   < Output")]
    [SerializeField] SteamVR_Action_Vibration vibration;


    [Header("- Caster - ")]
    [SerializeField] Caster caster;

    [Header("- Spells - ")]
    [SerializeField] VRSpellData[] spells;
    [SerializeField] VRHand leftHand;
    [SerializeField] VRHand rightHand;

    [Header("- Dev -")]
    [SerializeField] bool recordSpells;

    [Header("Markers")]
    [SerializeField] LayerMask mask;
    [SerializeField] float maxDistance;
    [SerializeField] float snapMinDistance;

    /// <summary>
    /// What shape triggers what action.
    /// </summary>
    Dictionary<Spell, VRSpellData> spellActions;

    /// <summary>
    /// Inspector mode: Player aims at something on the map
    /// </summary>
    bool inspectorMode;
    /// <summary>
    /// Throw mode: Player throw projectiles
    /// </summary>
    bool throwMode;

    /// <summary>
    /// Hand that is used in throw mode
    /// </summary>
    VRHand throwHand;

    /// <summary>
    /// Evaluates if the inspector button press was successful
    /// </summary>
    Func<bool> evaluateMode;

    float castTimeStamp;

    int spellIndex;

    public void Initialize()
    {
        var gManager = GameServices.Inst.GameManager;
        var teamSpots = gManager.GetSpawnSpots(GameManager.PlayerTeam);

        //Initializes VR
        VRRig.SetActive(true);
        VRRig.transform.position = teamSpots.PlayerRigSpot.position;
        VRRig.transform.rotation = teamSpots.PlayerRigSpot.rotation;

        //Magic, TODO: Ask Igor if it's necessary
        SteamVR_Input_Sources src = SteamVR_Input_Sources.Any;
        actionSet.Activate(src, 999, true);

        //Paring spells shapes with data
        spellActions = spells.ToDictionary(x => x.Spell, x => x);

        //When spell is recognized
        caster.OnSpellCasted += (spell, vrHand) =>
        {
            VRSpellData data = null;
            VRHand hand = (VRHand)vrHand;

            vibration.Execute(0, 0.25f, 100, 0.25f, hand.Source);

            hand.CastEffect();

            if (spellActions.TryGetValue(spell, out data))
            {
                //Dumpper
                castTimeStamp = Time.time + 0.25f;
                ProcessAction(data, hand);
            }
            else
                Debug.Log("Unregistered action " + spell.id);
        };
        
        // ---------

        leftHand.Source = SteamVR_Input_Sources.LeftHand;
        rightHand.Source = SteamVR_Input_Sources.RightHand;

        caster.Initialize();
    }

    /// <summary>
    /// Defines if any nessesery mode needs to be turned on.
    /// </summary>
    /// <param name="data">Data about the spell</param>
    /// <param name="hand">Hand</param>
    void ProcessAction(VRSpellData data, VRHand hand)
    {
        var gManager = GameServices.Inst.GameManager;

        var action = data.Action;

        if (!gManager.IsAvailable(data.Action))
            return;

        if(data.InspectorMode)
        {
            evaluateMode = () => { return gManager.ExecuteAction(action, hand.Marker.position, hand.Marker.rotation); };
            inspectorMode = true;
        }
        else if(data.ThowMode)
        {
            Debug.LogFormat("{0} >>> ThrowMode: {1}", Log.Dye("Debug", 1), data.Spell.name);

            if (gManager.ExecuteProjectileAction(action, hand)) 
            { 
                evaluateMode = () => true;
                throwHand = hand;
                throwMode = true;
            }
        }
        else
            gManager.ExecuteAction(action, hand.Marker.position, hand.Marker.rotation);
    }
    
    public void Update()
    {
        if (Time.time < castTimeStamp)
            return;

        //Dev option to record spells
        if (recordSpells)
        {
            CheckRecord(dpad, leftHand);
            CheckRecord(dpad, rightHand);
        }

        //If the didn't start
        if (!GameServices.Inst.GameManager.IsReady)
            return;

        //Inspector
        if (inspectorMode)
        {
            leftHand.SetMarker(true);
            rightHand.SetMarker(true);

            CheckForInspector(grip, leftHand);
            CheckForInspector(grip, rightHand);

            PlaceMarkers(leftHand);
            PlaceMarkers(rightHand);
        }
        //throw
        else if (throwMode && throwHand != null)
        {
            //Anti-spam
            if(GetButtonUp(trigger, throwHand.Source))
            {
                if (evaluateMode())// not nessesry ... () => true
                {
                    throwHand.DetatchProjectile();
                    throwMode = false;
                    evaluateMode = null;
                }
            }
        }
        //cast
        else
        {
            leftHand.SetMarker(false);
            rightHand.SetMarker(false);

            CheckSpell(trigger, leftHand);
            CheckSpell(trigger, rightHand);

            if (GetButtonDown(grip, leftHand.Source) || GetButtonDown(grip, rightHand.Source))
            {
                spellIndex += 1;
                if (spellIndex == spells.Count())
                    spellIndex = 0;
                var data = spells[spellIndex];
                GameServices.Inst.GameManager.ShowSpellHelp(data.Action, data.Spell);
            }
        }

        //anti-error state. Disables all modes and returns to casting. 
        if(GetButtonUp(menu, leftHand.Source) || GetButtonUp(menu, rightHand.Source))
            throwMode = inspectorMode = false;
    }

    RaycastHit hit;
    /// <summary>
    /// Positions the 3D markers when called
    /// </summary>
    /// <param name="hand">Hand for it's reference to the marker</param>
    void PlaceMarkers(VRHand hand)
    {
        Vector3 pos = hand.transform.position;
        Vector3 dir = hand.transform.forward;

        if (Physics.Raycast(pos, dir, out hit, maxDistance, mask))
        {
            Vector3 p = hit.point;
            hand.SetMarker(true);
            p = snapMinDistance > Vector3.Distance(p, hit.point) ? p : hit.point;
            hand.SetMarker(p);
        }
        else
        {
            hand.SetMarker(false);
        }
    }

    /// <summary>
    /// Based on the state of "cast button" caster is called.
    /// </summary>
    /// <param name="action">button</param>
    /// <param name="hand">hand</param>
    void CheckSpell(SteamVR_Action_Boolean action, VRHand hand)
    {
        var source = hand.Source;
        if(GetButton(action, source))
        {
            hand.SetTrail(true);
            caster.ContinueCast(hand.Position);
        }
        if(GetButtonUp(action, source))
        {
            hand.SetTrail(false);
            caster.EndCast(hand.Position, hand);
        }
        if (GetButtonDown(action, source))
        {
            hand.SetTrail(true);
            caster.BeginCast(hand.Position);
        }
    }

    /// <summary>
    /// Based on the state of "record button" caster is called.
    /// </summary>
    /// <param name="action">button</param>
    /// <param name="hand">hand</param>
    void CheckRecord(SteamVR_Action_Boolean action, VRHand hand)
    {
        var source = hand.Source;
        if (GetButton(action, source))
        {
            hand.SetTrail(true);
            caster.ContinueRecording(hand.Position);
        }
        if (GetButtonUp(action, source))
        {
            hand.SetTrail(false);
            caster.EndRecording(hand.Position);
        }
        if (GetButtonDown(action, source))
        {
            hand.SetTrail(true);
            caster.BeginRecording(hand.Position);
        }
    }
    
    /// <summary>
    /// Called while inspector mode is on
    /// </summary>
    /// <param name="action">Button</param>
    /// <param name="source">Source</param>
    /// <param name="hand">Hand</param>
    void CheckForInspector(SteamVR_Action_Boolean action, VRHand hand)
    {
        if(GetButtonUp(action, hand.Source))
        {
            //If true, inspector was successful.
            if (evaluateMode != null && evaluateMode())
            {
                leftHand.SetMarker(false);
                rightHand.SetMarker(false);

                inspectorMode = false;
                evaluateMode = null;
            }
        }
    }

    //Basic inputs, OnState, OnStateUp, OnStateDown
    bool GetButton(SteamVR_Action_Boolean action, SteamVR_Input_Sources source)
    {
        return action.GetState(source);
    }
    bool GetButtonDown(SteamVR_Action_Boolean action, SteamVR_Input_Sources source)
    {
        return action.GetStateDown(source);
    }
    bool GetButtonUp(SteamVR_Action_Boolean action, SteamVR_Input_Sources source)
    {
        return action.GetStateUp(source);
    }
}

/// <summary>
/// Data about the bond
/// </summary>
[Serializable]
public class VRSpellData
{
    public Spell Spell;
    public PlayerAction Action;
    public bool ThowMode;
    public bool InspectorMode;
}