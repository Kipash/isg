﻿using Photon.Pun;
using System;
using UnityEngine;

[Serializable]
public class DevControls : IControls, IUpdateable
{
    public Camera DevCam;
    [SerializeField] float aiTickRate;

    bool ai;
    float tickTimeStamp;

    void AITick()
    {
        //var team = PhotonNetwork.IsMasterClient ? Team.blu : Team.red;
        //var types = new EntityType[] { EntityType.Archer, EntityType.Knight, EntityType.Pikeman, EntityType.Catapult };
        //var type = types[UnityEngine.Random.Range(0, types.Length)];
        //var spots = GameServices.Inst.GameManager.GetSpawnSpots(team).UnitSpots;
        //var spot = spots[UnityEngine.Random.Range(0, spots.Length)];
        //
        //SpawnUnit(type, team, spot.position, spot.rotation);
    }

    void SpawnUnit(PlayerAction action, Vector3 position, Quaternion rotation)
    {
        GameServices.Inst.GameManager.ExecuteAction(action, position, rotation);
    }

    public void Initialize()
    {
        DevCam.gameObject.SetActive(true);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            OnClick(PlayerAction.SpawnKnight);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            OnClick(PlayerAction.SpawnPikeman);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            OnClick(PlayerAction.SpawnArcher);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            OnClick(PlayerAction.SpawnCatapult);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            OnClick(PlayerAction.SpawnTower);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            OnClick(PlayerAction.PowerBuff);
        }
    }
    
    void OnClick(PlayerAction a)
    {
        if (GameServices.Inst.GameManager.IsReady)
        {
            var pos = DevCam.ScreenToWorldPoint(Input.mousePosition);
            pos.y = 0;

            var team = PhotonNetwork.IsMasterClient ? Team.blu : Team.red;

            SpawnUnit(a, pos, Quaternion.identity);
        }
    }
}