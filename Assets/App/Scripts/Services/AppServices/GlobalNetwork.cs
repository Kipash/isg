﻿using System;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

/// <summary>
/// Zprostředkovává připojení k Photonu.
/// </summary>
[Serializable]
public class GlobalNetwork : IInitializable
{
    GlobalNetworkCallBacks callbacks = new GlobalNetworkCallBacks();

    [SerializeField] string region;

    /// <summary>
    /// On connected to Photon
    /// </summary>
    public Action Ready;
    public bool IsReady { get; private set; }

    public void Initialize(object i)
    {
        PhotonNetwork.AddCallbackTarget(callbacks);

        callbacks._OnConnectedToMaster += () => {
            Log.Write("Connected to Photon as /// " +
            	Log.Dye(PhotonNetwork.LocalPlayer.NickName, 4));

            if(Ready != null)
                Ready.Invoke();

            IsReady = true;
        };
    }

    /// <summary>
    /// Připojí se na Photon.
    /// </summary>
    public void ConnectToMaster()
    {
        PhotonNetwork.GameVersion = "S301 4ever";
        PhotonNetwork.ConnectUsingSettings();
    }
}