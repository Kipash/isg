﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Pool na entity, lze dostat volnou entitu na základě EntityType.
/// </summary>
[Serializable]
public class EntityPool : IInitializable
{
    [SerializeField] EntityPoolData[] poolData;
    [Space(10)]
    [SerializeField] Transform poolRoot;

    /// <summary>
    /// Všechny entity podle Typu.
    /// </summary>
    Dictionary<EntityType, EntityHolder[]> entities = new Dictionary<EntityType, EntityHolder[]>();
    /// <summary>
    /// Párovaní instance s holderem.
    /// </summary>
    Dictionary<Entity, EntityHolder> instances = new Dictionary<Entity, EntityHolder>();

    /// <summary>
    /// Zdali bude vypisovat debug.
    /// </summary>
    [SerializeField] bool verbose;

    /// <summary>
    /// Vytvoří entity podle počtu definovaný v EntityPoolData.
    /// </summary>
    /// <param name="instance">Instance AppServices.</param>
    public void Initialize(object instance)
    {
        if (verbose)
            Log.Write(Log.Dye("EntityPool", Color.green) + "|> Initialize");
        foreach(var x in poolData)
        {
            if (verbose)
                Log.WriteF(Log.Dye("EntityPool", Color.green) + "|> Creating {0} {1} times", x.Name, x.Amount);

            var buffer = new EntityHolder[x.Amount];
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = new EntityHolder();
                buffer[i].Instance = GameObject.Instantiate(x.EntityPrefab, poolRoot);
                buffer[i].IsAvailable = true;
                buffer[i].Instance.Deactivate();

                instances.Add(buffer[i].Instance, buffer[i]);
            }

            entities.Add(x.Type, buffer);
        }
    }

    /// <summary>
    /// Vrátí aktivní entitu do poolu.
    /// </summary>
    /// <returns><c>true</c>, pokud entita byla úspěšně vrácena, <c>false</c> pokud ne.</returns>
    /// <param name="entity">Entita kterou chceme vrátit.</param>
    public bool DeactivateEntity(Entity entity)
    {

        if (instances.ContainsKey(entity))
        {
            if (verbose)
                Log.WriteF(Log.Dye("EntityPool", Color.green) + "|> Returning {0}", entity.name);

            entity.transform.SetParent(poolRoot);
            entity.transform.position = Vector3.zero;
            entity.transform.rotation = Quaternion.identity;
            entity.transform.localScale = Vector3.one;

            instances[entity].IsAvailable = true;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Vrátí volnou enititu, pokud není vyhodí erorr a vráti null
    /// </summary>
    /// <returns>Vráti entitu nebo null.</returns>
    /// <param name="type">Typ entity.</param>
    public Entity GetEntity(EntityType type)
    {
        var e = GetAvailable(entities[type]);
        if (e != null)
        {
            e.transform.SetParent(null);
            return e;
        }
        else
        {
            Debug.LogError("Can't find avaiable instace of " + type);
            return null;
        }
    }

    /// <summary>
    /// Vrátí první entitu co je <c>IsAvailable</c>
    /// </summary>
    /// <returns>Volnou entitu.</returns>
    /// <param name="entities">Všechny entity</param>
    Entity GetAvailable(EntityHolder[] entities)
    {
        for (int i = 0; i < entities.Length; i++)
        {
            if (entities[i].IsAvailable)
            {
                entities[i].IsAvailable = false;
                return entities[i].Instance;
            }
        }

        return null;
    }
}

/// <summary>
/// Spojuje referenci na instanci entity a informace o té isntaci, např. zdali je volná.
/// </summary>
[Serializable]
public class EntityHolder
{
    public bool IsAvailable;
    public Entity Instance;
}

/// <summary>
/// Základní informace pro pool.
/// - Názdev.
/// - Typ pod kterým se instance dá najít.
/// - Reference na prefab.
/// - Počet kolik kopii se má udělat.
/// </summary>
[Serializable]
public class EntityPoolData
{
    public string Name;
    public EntityType Type;
    public Entity EntityPrefab;
    public int Amount;
}

/// <summary>
/// Kategorie entity, na příklad letectvo, pěchota ... 
/// </summary>
public enum EntityClass
{
    none = 0,
    Building = 1,
    Aircraft = 2,
    Infantry = 3,
    Specture = 10,
}

/// <summary>
/// Konkrétní druh entity, na příklad hrad, rytíř ...
/// </summary>
public enum EntityType
{
    none = 0,

    Castle = 1,
    Tower = 2,

    Knight = 100,
    Pikeman = 101,

    Archer = 200,
    Catapult = 201,

    //Balloon = 300,
    //beh .. Igor = 301,

    //PowerBuff = 500,

    Spear = 600,
    Bolt = 601,

}