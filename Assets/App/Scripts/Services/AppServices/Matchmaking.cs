﻿using System;
using Photon.Pun;
using Photon.Realtime;

/// <summary>
/// Matchmaking.
/// </summary>
[Serializable]
public class Matchmaking : IInitializable
{
    MatchmakingCallBacks callBacks = new MatchmakingCallBacks();

    /// <summary>
    /// Event, jakmile se lokální hráč připojí do místnosti.
    /// </summary>
    public Action<Room> OnJoinedRoom;


    public void Initialize(object i)
    {
        PhotonNetwork.AddCallbackTarget(callBacks);

        callBacks._OnJoinedRoom += () => {
            var pl = PhotonNetwork.LocalPlayer;

            Log.Write("Connected to " +
                Log.Dye(PhotonNetwork.CurrentRoom.Name, 2) +
            	" as " + Log.Dye(pl.ActorNumber.ToString(), 3) +
                (pl.IsMasterClient ? Log.Dye(" (Master)", 6) : ""));

            if(OnJoinedRoom != null)
                OnJoinedRoom.Invoke(PhotonNetwork.CurrentRoom);
        };
    }

    /// <summary>
    /// Připojí se do místnosti, pokud neexistuje, založí ji. Maximální počet hráčů je 2.
    /// </summary>
    /// <param name="name">Název místnosti</param>
    public void JoinOrCreate(string name)
    {
        PhotonNetwork.JoinOrCreateRoom(name, new RoomOptions() { MaxPlayers = 2 }, null);
    }
}