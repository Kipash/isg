﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;
using Model;
using PathFind;
using Backend;
using UnityToolbag;

/// <summary>
/// Generuje mapu a dělá operace pro naleznutí políčka na určité pozici.
/// </summary>
[Serializable]
public class MapHexManager : IInitializable
{
    /// <summary>
    /// Odsazení mapy
    /// </summary>
    [SerializeField] Vector3 offset;
    /// <summary>
    /// samotná dlaždice
    /// </summary>
    [SerializeField] TileBehaviour tilePrefab;
    
    /// <summary>
    /// Odsazení dlaždic od sebe
    /// </summary>
    public const float Spacing = 1.5f * 0.8f;

    /// <summary>
    /// Pro rychlejší vyhledávaní uchování kde jaký Tile v reálném světe je.
    /// </summary>
    Dictionary<Tile, Vector3> tilePositions = new Dictionary<Tile, Vector3>();
    /// <summary>
    /// Tile v 2D mapě
    /// </summary>
	public TileBehaviour[,] Tiles;
    /// <summary>
    /// Mapa obsahující Tiles
    /// </summary>
    public Map Map;

    /// <summary>
    /// Vytvoří mapu z dat které jsou načténé z konfigu.
    /// </summary>
    /// <param name="instance">Instance GameServices.</param>
    public void Initialize(object instance)
    {
        LoadBoard(DataStorage.ProgramData.MapData);
        foreach(var x in Tiles)
        {
            if(x != null)
                tilePositions.Add(x.Tile, x.transform.position);
        }
    }

    /// <summary>
    /// Generace mapy
    /// </summary>
    /// <param name="data">Data definující všechny Tiles</param>
    public void LoadBoard(MapData data)
    {
        var mapRoot = new GameObject(" ~ Hex map ~ ").transform;
        Map = new Map(data.Width, data.Height);
        Tiles = new TileBehaviour[data.Width, data.Height];
        
        foreach (var tileData in data.Tiles)
        {
            SetupTile(tileData, mapRoot, Map, Tiles);
        }
    }
    
    /// <summary>
    /// Vytvoří jeden tile
    /// </summary>
    /// <param name="data">Informace o Tile</param>
    /// <param name="root">Rodič pod kterým má GameObject být</param>
    /// <param name="map">Reference na mapa, aby se tam mohl přidat</param>
    /// <param name="tiles">Reference na 2D mapu</param>
    void SetupTile(TileData data, Transform root, Map map, TileBehaviour[,] tiles)
    {
        var tile = GameObject.Instantiate(tilePrefab);
        var x = data.X;
        var y = data.Y;
        var depth = data.Depth;
         
        tiles[x, y] = tile;

        tile.gameObject.name = string.Format("Hex tile ({0}, {1})", x, y);

        tile.transform.parent = root.transform;
        tile.transform.position = GenerateCoordinates(x, y);

        tile.Tile = map.GameBoard[x, y];
        tile.Tile.Depth = depth;

        tile.SetMaterial();
    }

    /// <summary>
    /// Vygeneruje souřadnice na základě Spacing.
    /// </summary>
    /// <param name="x">X na mapě</param>
    /// <param name="y">Y na mapě</param>
    /// <returns>Vrátí pozici v reálném světě</returns>
    Vector3 GenerateCoordinates(int x, int y)
    {
        var zOffset = x % 2 == 0 ? 0 : -Spacing / 2;
        return new Vector3(x * Spacing + offset.x, offset.y, y * Spacing + zOffset + offset.z);
    }
    
    /// <summary>
    /// Získá reálnou pozici ve světě.
    /// </summary>
    /// <param name="p">Pozice na mapě</param>
    /// <returns></returns>
    public Vector3 GetWorldCoordinates(Point p)
    {
        return GetWorldCoordinates(p.X, p.Y);
    }
    /// <summary>
    /// Získá reálnou pozici ve světě.
    /// </summary>
    /// <param name="x">X pozice na mapě</param>
    /// <param name="y">Y pozice na mapě</param>
    /// <returns>Pokud nic nenalezne, vratí se defaltní hodnota (-100, -100, -100)</returns>
    public Vector3 GetWorldCoordinates(int x, int y)
    {
        var w = Tiles.GetLength(0);
        var h = Tiles.GetLength(1);
        if ((x < 0 || x >= w) || (y < 0 || y >= h))
            return Vector3.zero.GetDefault();
        else if (Tiles[x, y] == null)
            return Vector3.zero.GetDefault();
        else
            return Tiles[x, y].transform.position;
    }
    /// <summary>
    /// Získá reálnou pozici ve světě.
    /// </summary>
    /// <param name="tile">Tile na mapě</param>
    /// <returns>Pokud nic nenalezne, vratí se defaltní hodnota (-100, -100, -100)</returns>
    public Vector3 GetWorldCoordinates(Tile tile)
    {
        return tile == null ? Vector3.zero.GetDefault() : tilePositions[tile];
    }

    /// <summary>
    /// Asynchroně získá Tiles podle pozic v reálném světe.
    /// </summary>
    /// <param name="positions">Pozice v reálném světě.</param>
    /// <returns>Vrací asynchroní objekt který sedá spustit a vyvolá callback po dokončení</returns>
    Future<Tile[]> GetTilesAsync(Vector3[] positions)
    {
        var calc = new Future<Tile[]>();

        calc.Process(() =>
        {
            return positions
                .Select(x => GetTile(x))
                .ToArray();
        });
        
        return calc;
    }

    /// <summary>
    /// Asynchroní naleznutí cesty
    /// </summary>
    /// <param name="worldStart">Začátek cesty</param>
    /// <param name="worldEnd">Konec cesty</param>
    /// <param name="depth">Hloubka pro kolize</param>
    /// <param name="id">ID entity co hledá cestu</param>
    /// <param name="callback">Callback pro zavolání, jakmile se cesta najde</param>
    public void GetPathAsync(Vector3 worldStart, Vector3 worldEnd, int depth, int id, Action<Path<Tile>> callback)
    {
        var positions = new Vector3[] { worldStart, worldEnd };

        /*
        var start = GetTile(worldStart);
        var end = GetTile(worldEnd);
        
        Func<Tile, Tile, double> distance = (node1, node2) => 1;
        Func<Tile, double> estimate = t => Math.Sqrt(Math.Pow(t.X - start.X, 2) + Math.Pow(t.Y - start.Y, 2));
        
        var p = PathFind.PathFind.FindPath(start, end, depth, id, distance, estimate);
        
        callback(p);
        */

        var calc = GetTilesAsync(positions);

        calc.OnSuccess(x =>
        {
            GetPathAsync(x.value[0], x.value[1], depth, id, callback);
        });
    }

    /// <summary>
    /// Asynchroní naleznutí cesty
    /// </summary>
    /// <param name="startTile">Začáteční tile</param>
    /// <param name="endTile">Konečnej tile</param>
    /// <param name="depth">Hloubka pro kolize</param>
    /// <param name="id">ID entity co hledá cestu</param>
    /// <param name="callback">Callback pro zavolání, jakmile se cesta najde</param>
    public void GetPathAsync(Tile startTile, Tile endTile, int depth, int id, Action<Path<Tile>> callback)
    {
        Func<Tile, Tile, double> distance = (node1, node2) => 1;
        Func<Tile, double> estimate = t => Math.Sqrt(Math.Pow(t.X - endTile.X, 2) + Math.Pow(t.Y - endTile.Y, 2));

        var pathCalc = new Future<Path<Tile>>();
        
        pathCalc.Process(() =>
        {
            //Swaping start with end - IEnumerator is logicaly reversed
            return PathFind.PathFind.FindPath(startTile, endTile, depth, id, distance, estimate);
        });

        pathCalc.OnSuccess((x) =>
        {
            callback(x.value);
        });
    }

    /// <summary>
    /// Získá tile podle pozice.
    /// </summary>
    /// <param name="worldPosition">Pozice ve světě</param>
    /// <returns></returns>
    public Tile GetTile(Vector3 worldPosition)
    {
        float minDistance = float.MaxValue;
        int finalX = -1;
        int finalY = -1;
        
        for (int x = 0; x < Tiles.GetLength(0); x++)
        {
            for (int y = 0; y < Tiles.GetLength(1); y++)
            {
                var tb = Tiles[x, y];
                if (tb == null || tb.Tile == null)
                    continue;

                Vector3 tilePosition = GetWorldCoordinates(tb.Tile);

                float dist = Vector3.Distance(worldPosition, tilePosition);
                if (dist < minDistance)
                {
                    minDistance = dist;
                    finalX = x;
                    finalY = y;
                }
            }
        }

        if (finalX == -1 || finalY == -1)
            return null;

        var finalTile = Tiles[finalX, finalY].Tile;

        return finalTile;
    }

    /// <summary>
    /// Podle pozice v reálném světe najde pozici tilu a tu reálnou upraví
    /// </summary>
    /// <param name="pos">Pozice v reálném světě</param>
    /// <returns>True - nalez, False - nenalezl</returns>
    public bool SnapVector3(ref Vector3 pos)
    {
        var t = GetTile(pos);

        //TODO: add depth filters
        //TODO: trashold / sensitivity

        if (t != null)
        {
            pos = GetWorldCoordinates(t.Location);
            return true;
        }
        else
            return false;
    }
}
