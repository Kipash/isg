﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

/// <summary>
/// Implementuje eventy:
/// 
/// OnMasterClientSwitched
/// OnPlayerEnteredRoom
/// OnPlayerLeftRoom
/// OnPlayerPropertiesUpdate
/// OnRoomPropertiesUpdate
/// </summary>
[Serializable]
public class RoomNetwork : IInitializable
{
    public Action<RoomNetwork> Ready;
    public Action<Player> PlayerConnected;
    public Action<Player> PlayerDisconnected;

    RoomNetworkCallBacks callBacks = new RoomNetworkCallBacks();
    public void Initialize(object i)
    {
        PhotonNetwork.AddCallbackTarget(callBacks);

        callBacks._OnPlayerEnteredRoom += (Player p) => { 
            Log.Write(p.NickName + "connected");

            if(PlayerConnected != null)
                PlayerConnected.Invoke(p); 
        };

        callBacks._OnPlayerLeftRoom += (Player p) => { 
            Log.Write(p.NickName + " disconnected");

            if(PlayerDisconnected != null)
                PlayerDisconnected.Invoke(p);
        };

        callBacks._OnMasterClientSwitched += (Player obj) => {
            //TODO: not
            Application.Quit();
        };
    }
}
