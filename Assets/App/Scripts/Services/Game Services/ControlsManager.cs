﻿using System;
using UnityEngine;
using UnityEngine.XR;

/// <summary>
/// Přepína mezi VR a PC ovládaním.
/// </summary>
[Serializable]
public class ControlsManager : IInitializable, IUpdateable
{
    [SerializeField] bool forceDevControls;

    [SerializeField] VRControls VRControls;
    [SerializeField] DevControls DevControls;

    [HideInInspector] public Transform HeadTarget;

    IControls controls;
    IUpdateable updateableControls;

    /// <summary>
    /// Přepne podle XRDevice.isPresent, nebo si vynutí PC ovládaní
    /// </summary>
    /// <param name="instance"></param>
    public void Initialize(object instance)
    {
        var vrPresent = forceDevControls ? false : XRDevice.isPresent;

        object c = null;
        if (vrPresent)
        {
            HeadTarget = VRControls.Neck;
            c = VRControls;
        }
        else
        {
            HeadTarget = DevControls.DevCam.transform;
            c = DevControls;
        }

        controls = (IControls)c;
        controls.Initialize();
        updateableControls = (IUpdateable)c;
    }

    public void Update()
    {
        if (updateableControls != null)
            updateableControls.Update();
    }
}