﻿using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Spravuje 3D avatry lokálního a cizího hráče.
/// </summary>
[Serializable]
public class VRPlayerManager : IInitializable , ILateUpdateable
{
    [SerializeField] Transform headset;
    [SerializeField] Transform leftController;
    [SerializeField] Transform rightController;

    [SerializeField] float syncRate;
    
    [Header("Player IKs")]
    [SerializeField] PlayerIK localPlayer;
    [SerializeField] PlayerIK otherPlayer;

    [Header("Player hands")]
    [SerializeField] VRHand leftHand;
    [SerializeField] VRHand rightHand;

    [Header("Fake player hands")]
    [SerializeField] VRHand fakeLeftHand;
    [SerializeField] VRHand fakeRightHand;

    bool receivedUpdate;

    float syncTimestamp;
    float syncProgress;

    VRPlayerData oldPacket;
    VRPlayerData newPacket;

    /// <summary>
    /// Nastaví callbacku pro VRPlayerUpdated event
    /// </summary>
    /// <param name="instance">Instance GameServices</param>
    public void Initialize(object instance)
    {
        GameServices.Inst.EventManager.Processors.Add(NetworkEvent.VRPlayerUpdated, (x) =>
        {
            receivedUpdate = true;

            var data = (VRPlayerData)x;

            syncProgress = 0;

            oldPacket = newPacket;
            newPacket = data;
        });
    }
    
    /// <summary>
    /// IK pro lokálního hráče, vezme pozice hlavy a ovladačů a napne mezi ně tělo
    /// </summary>
    /// <param name="player">Hrač který nastaví</param>
    void ProcessPlayer(PlayerIK player)
    {
        player.ProcessIK(headset.position,         headset.localRotation,
                         leftController.position,  leftController.localRotation,
                         rightController.position, rightController.localRotation);
    }

    /// <summary>
    /// IK pro hráče, vezme pozice hlavy a ovladačů a napne mezi ně tělo
    /// </summary>
    /// <param name="player">Hráč kterému to nastaví</param>
    /// <param name="data">Data podle kterých to nastaví</param>
    void ProcessPlayer(PlayerIK player, VRPlayerData data)
    {
        player.ProcessIK(data.HeadsetPosition.Vector,
                         data.HeadsetRotation.Quaternion,
                         data.LeftControllerPosition.Vector,
                         data.LeftControllerRotation.Quaternion,
                         data.RightControllerPosition.Vector,
                         data.RightControllerRotation.Quaternion);
    }

    /// <summary>
    /// Vyhlazeně nastaví hráče podle dat.
    /// </summary>
    /// <param name="player">Hráč</param>
    /// <param name="newData">Nové data</param>
    /// <param name="oldData">Staré data</param>
    /// <param name="t">Přechod</param>
    void ProcessPlayer(PlayerIK player, VRPlayerData newData, VRPlayerData oldData, float t)
    {
        player.ProcessIK(Vector3.Lerp(      oldData.HeadsetPosition.Vector,                 newData.HeadsetPosition.Vector, t),
                         Quaternion.Slerp(  oldData.HeadsetRotation.Quaternion,             newData.HeadsetRotation.Quaternion, t),
                         Vector3.Lerp(      oldData.LeftControllerPosition.Vector,          newData.LeftControllerPosition.Vector, t),
                         Quaternion.Slerp(  oldData.LeftControllerRotation.Quaternion,      newData.LeftControllerRotation.Quaternion, t),
                         Vector3.Lerp(      oldData.RightControllerPosition.Vector,         newData.RightControllerPosition.Vector, t),
                         Quaternion.Slerp(  oldData.RightControllerRotation.Quaternion,     newData.RightControllerRotation.Quaternion, t));
    }

    /// <summary>
    /// Vyhlazovaní hráče
    /// </summary>
    public void LateUpdate()
    {
        if(syncTimestamp < Time.time)
        {
            syncTimestamp = Time.time + syncRate;
            
            GameServices.Inst.EventManager.Raise(NetworkEvent.VRPlayerUpdated, new VRPlayerData()
            {
                HeadsetPosition = new V3(headset.position),
                HeadsetRotation = new Q(headset.localRotation),
                LeftControllerPosition = new V3(leftController.position),
                LeftControllerRotation = new Q(leftController.localRotation),
                RightControllerPosition = new V3(rightController.position),
                RightControllerRotation = new Q(rightController.localRotation),
            });
        }

        if (oldPacket != null && newPacket != null)
        {
            syncProgress += Time.deltaTime;
            ProcessPlayer(otherPlayer, newPacket, oldPacket, syncProgress / syncRate);
        }
        else if (newPacket != null)
        {
            ProcessPlayer(otherPlayer, newPacket);
        }



        ProcessPlayer(localPlayer);
    }

    /// <summary>
    /// Umístění házitelných objektů do rukou
    /// </summary>
    /// <param name="t">Objekt</param>
    /// <param name="hand">ruka, leva = 0, prava = 1</param>
    /// <param name="fake">zdali se to má objevit u opačného hráče</param> 
    public void Mount(Transform t, int hand, bool fake) //TODO: change hand Index to enum, reconsider fake concept
    {
        if (fake)
        {
            if (hand == 0)
                fakeLeftHand.Mount(t);
            else if (hand == 1)
                fakeRightHand.Mount(t);
        }
        else
        {
            if (hand == 0)
                leftHand.Mount(t);
            else if (hand == 1)
                rightHand.Mount(t);
        }
    }
}

/// <summary>
/// Synchronizovatelný data o hlavě a rukou.
/// </summary>
[Serializable]
public class VRPlayerData
{
    public V3 HeadsetPosition;
    public Q HeadsetRotation;
    public V3 LeftControllerPosition;
    public Q LeftControllerRotation;
    public V3 RightControllerPosition;
    public Q RightControllerRotation;
}
