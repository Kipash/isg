﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Drží manu a pomáha zpustit hru, měl by držet stav hry a vše co je s ní spojeno.
/// </summary>
[Serializable]
public class GameManager : IInitializable, IUpdateable
{
    [Header("Spells")]
    [SerializeField] ActionData[] actionData;

    [Header("Mana")]
    [SerializeField] float manaTickrate;
    [SerializeField] int manaIncrease;
    [SerializeField] int maxMana;
    [SerializeField] int localMana; //temp / mock

    [Header("UI")]
    [SerializeField] Text manaText;
    [SerializeField] Text spellName;
    [SerializeField] Text spellCost;

    [Header("Spell visualizers")]
    [SerializeField] SpellVisualizer spellVisualized;

    public int LocalMana
    {
        get { return localMana; }
        set { localMana = value; manaText.text = string.Format("{0} mana", LocalMana); }
    }
    float manaTimestamp;


    Dictionary<PlayerAction, ActionData> actions;

    /// <summary>
    /// Reference na Photonovskou proměnou hráče.
    /// </summary>
    public Player Player { get { return PhotonNetwork.LocalPlayer; } }

    /// <summary>
    /// Lokální tým
    /// </summary>
    public static Team PlayerTeam { get { return PhotonNetwork.IsMasterClient ? Team.blu : Team.red; } }
    /// <summary>
    /// Nepřátelský tým
    /// </summary>
    public static Team InvertedPlayerTeam { get { return PhotonNetwork.IsMasterClient ? Team.red : Team.blu; } }

    /// <summary>
    /// Pokud hra odstartovala
    /// </summary>
    public bool IsReady { get; private set; }
    /// <summary>
    /// Event, který hru nastartuje. Je volán po tom co se oba klienti načtou.
    /// </summary>
    public Action Ready;


    /// <summary>
    /// VR avatar nepřítele.
    /// </summary>
    [Header("References")]
    [SerializeField] GameObject VRFakeRig;

    /// <summary>
    /// Spawnspoty pro modrý tým.
    /// </summary>
    [SerializeField] TeamSpawnSpots bluSpawns;
    /// <summary>
    /// Spawnspoty pro červený tým.
    /// </summary>
    [SerializeField] TeamSpawnSpots redSpawns;

    /// <summary>
    /// Zajistí spuštění Ready eventu
    /// </summary>
    /// <param name="i">reference na GameServices</param>
    public void Initialize(object i)
    {
        Ready += SetupClient;

        AppServices appServ = AppServices.Inst;

        if (!Player.IsMasterClient)
        {
            GameServices.Inst.EventManager.Raise(NetworkEvent.GameReady, new object(), ReceiverGroup.MasterClient);
            Ready();
        }
        else
        {
            GameServices.Inst.EventManager.Processors.Add(NetworkEvent.GameReady, (x) =>
            {
                Ready();
            });
        }
    }

    /// <summary>
    /// Nastaví klienta a patřičné věci podle role (Master / Regular)
    /// </summary>
    void SetupClient()
    {
        IsReady = true;
        AppServices appServ = AppServices.Inst;

        Transform spot = GetSpawnSpots(InvertedPlayerTeam).PlayerRigSpot;
        VRFakeRig.SetActive(true);
        VRFakeRig.transform.position = spot.position;
        VRFakeRig.transform.rotation = spot.rotation;

        //Fills actionas based on actiodata. PlayerAction is the key, ActionData is the value.
        actions = actionData.ToDictionary(x => x.Action, x => x);
        SetupActionExecution();

        if (Player.IsMasterClient)
        {
            Log.Write(" >> Setting up client as " + Log.Dye("Master", 6));

            SpawnEntity(EntityType.Castle, Team.blu, bluSpawns.CastleSpawnSpot);
        }
        else
        {
            Log.Write("Setting up client as " + Log.Dye("Regular", 4));

            SpawnEntity(EntityType.Castle, Team.red, redSpawns.CastleSpawnSpot);
        }
    }

    void SpawnEntity(EntityType type, Team team, Transform trans)
    {
        GameServices.Inst.EntityManager.CreateEntity(new EntityData()
        {
            Team = team,
            Type = type,
            InitialPosition = new V3(trans.position),
            InitialRotation = new Q(trans.rotation)
        });
    }

    //Jdete vsichni dopici...
    void SetupActionExecution()
    {
        Func<ActionData, Vector3, Quaternion, bool> spawnEntity = (data, pos, rot) =>
        {
            var units = GameServices.Inst.EntityManager.Entities
                            .Values
                            .Where(x => x is Unit)
                            .Cast<Unit>();
                                ;

            foreach (var unit in units)
            {
                //TODO: reference for this entity
                if (Vector3.Distance(pos, unit.transform.position) < unit.Agent.radius * 2)
                    return false;
            }


            GameServices.Inst.EntityManager.CreateEntity(new EntityData()
            {
                Team = PlayerTeam,
                Type = data.SpawnType,
                InitialPosition = new V3(pos),
                InitialRotation = new Q (rot)
            });
            return true;
        };

        //Simple spawns
        actions[PlayerAction.SpawnPikeman].Execute  = spawnEntity;
        actions[PlayerAction.SpawnArcher].Execute   = spawnEntity;
        actions[PlayerAction.SpawnKnight].Execute   = spawnEntity;
        actions[PlayerAction.SpawnCatapult].Execute = spawnEntity;
        actions[PlayerAction.SpawnTower].Execute    = spawnEntity;

        // ---------

        actions[PlayerAction.PowerBuff].Execute = (data, pos, rot) =>
        {
            int minDistance = 3;

            //filteres spectres and buildings
            var units = GameServices.Inst.EntityManager.Entities
                .Values
                .Where(w => w is Unit)
                .Cast<Unit>();

            //Buffs units in range 
            foreach (var x in units)
            {
                if (minDistance > Vector3.Distance(pos, x.transform.position))
                {
                    x.Buff();
                }
            }

            //if it found nay
            return units.Count() > 0;
        };

        //Can't be evaluated here. Disabled for now
        //actions[PlayerAction.Spear].Execute = actions[PlayerAction.Bolt].Execute = (x, y, z) => true;
        
        ///Note: Spear and Bolt as other spells aren't implemented the same way, since they depand on VRHand and VRPlayerManager.
    }
    
    public bool ExecuteAction(PlayerAction action, Vector3 position, Quaternion rotation)
    {
        //Debug.LogFormat("Pos: {0} ; Rot: {1}", position, rotation);

        ActionData data;
        if(actions.TryGetValue(action, out data))
        {
            var spot = GetSpawnSpot(PlayerTeam);
            var finalPos = data.SpawnSelect ? position : spot.position; 
            var finalRot = data.SpawnSelect ? rotation : spot.rotation;

            if (localMana >= data.ManaCost && data.Execute(data, finalPos, finalRot))
            {
                LocalMana -= data.ManaCost;

                Debug.LogFormat("> {0}: {1}", Log.Dye("Player action", 1), Log.Dye(data.About, 3));
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }

    Transform GetSpawnSpot(Team playerTeam)
    {
        var spots = GetSpawnSpots(playerTeam).UnitSpots;
        var index = UnityEngine.Random.Range(0, spots.Length);
        return spots[index];
    }

    public bool ExecuteProjectileAction(PlayerAction action, VRHand hand)
    {
        ActionData data;
        if (actions.TryGetValue(action, out data))
        {
            if (localMana >= data.ManaCost)
            {
                hand.InitializeThrow(actions[action].SpawnType);
                Debug.LogFormat("{0} >>> InitializeThrow: {1}", Log.Dye("Debug", 1), data.Action);
                localMana -= data.ManaCost;

                Log.WriteF("> {0}: {1}", Log.Dye("Player action", 1), Log.Dye(data.About, 3));
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }

    public bool IsAvailable(PlayerAction action)
    {
        ActionData data;
        if (actions.TryGetValue(action, out data))
            return data.ManaCost <= localMana;
        else
            return false;
    }


    /// <summary>
    /// Pro získání spawnspotů
    /// </summary>
    /// <param name="t">Tým</param>
    /// <returns>Vrátí SpawnSpoty</returns>
    public TeamSpawnSpots GetSpawnSpots(Team t)
    {
        if (t == Team.red)
        {
            return redSpawns;
        }
        else if (t == Team.blu)
        {
            return bluSpawns;
        }
        else
        {
            return null;
        }
    }

    void ManaTick()
    {
        LocalMana += manaIncrease;
        if (LocalMana > maxMana)
            LocalMana = maxMana;
    }

    public void Update()
    {
        if (manaTimestamp < Time.time && IsReady)
        {
            ManaTick();
            manaTimestamp = Time.time + manaTickrate;
        }
    }

    public void ShowSpellHelp(PlayerAction action, Spell shape)
    {
        spellVisualized.DisplaySpell(shape);

        var data = actions[action];
        spellCost.text = "Cost: " + Log.Dye(data.ManaCost.ToString(), 1);
        spellName.text = data.About;
    }
}

/// <summary>
/// Jednotlivé SpawnSpoty
/// </summary>
[Serializable]
public class TeamSpawnSpots
{
    public Transform PlayerRigSpot;
    public Transform CastleSpawnSpot;
    public Transform[] UnitSpots;
    public Transform[] TowerSpots;
}

[Serializable]
public class ActionData
{
    public PlayerAction Action;
    public EntityType SpawnType;
    //public Spell Shape;
    public int ManaCost;
    public string About;
    public bool SpawnSelect;

    /// <summary>
    /// <c>ActionData</c> - reference to the data
    /// <c>Vector3</c> - marker position
    /// <c>Quaternion</c> - marker rotation
    /// <c>bool</c> - returns true if everything went fine
    /// </summary>
    public Func<ActionData, Vector3, Quaternion, bool> Execute;
}