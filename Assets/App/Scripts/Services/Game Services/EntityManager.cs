﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spravuje entity, jejich spawnutí, částečnou synchronizaci a 
/// </summary>
[Serializable]
public class EntityManager : IInitializable
{
    /// <summary>
    /// Seznam všech entit podle ID
    /// </summary>
    public Dictionary<int, Entity> Entities = new Dictionary<int, Entity>();
    /// <summary>
    /// Ticket a callbacky, tickety jsou použity pro asynchroní spawnutí jednotky.
    /// Při požádaní o spawnutí se vytvoří ticket, po spawnutí se ověři jestli není nějaký ticket registrovát a callback se použije a smaže.
    /// </summary>
    Dictionary<int, Action<Entity>> ticketCallBacks = new Dictionary<int, Action<Entity>>();
    
    /// <summary>
    /// Nastavení callbacků Pro EventManager:
    /// EntityCreate
    /// EntityUpdated
    /// EntityDestroid
    /// EntityDemanded
    /// </summary>
    /// <param name="instance"></param>
    public void Initialize(object instance)
    {
        var eManager = GameServices.Inst.EventManager;

        //Master -> Regular 
        eManager.Processors.Add(NetworkEvent.EntityCreated, (x) =>
        {
            var data = (EntityData)x;

            Log.WriteF("{0} created entity with ID: {1}", Log.Dye("Master", 6), data.ID);

            SpawnEntity(data);
        });

        //Master -> Regular
        eManager.Processors.Add(NetworkEvent.EntityUpdated, (x) =>
        {
            var data = (EntityUpdateData)x;

            if (Entities.ContainsKey(data.EntityID))
                Entities[data.EntityID].UpdateEntity(data);
            else
                Debug.LogError("Updating unregistered entity: " + data.EntityID);
        });

        //Master -> regular
        eManager.Processors.Add(NetworkEvent.EntityDestroied, (x) =>
        {
            DestroyEntity((int)x);
        });

        //Regular -> Master
        eManager.Processors.Add(NetworkEvent.EntityDemanded, (x) =>
        {
            var data = (EntityData)x;

            Log.WriteF("{0} is demanding creating a entity", Log.Dye("Client", 4));
            
            SpawnEntity(data);
        });

    }
    
    /// <summary>
    /// Zabití entity, entita se deaktivuje a vrátí se změt do poolu.
    /// </summary>
    /// <param name="id">ID entity kterou chceme zabít</param>
    public void DestroyEntity(int id)
    {
        if (!Entities.ContainsKey(id))
            return;
        
        Entities[id].Deactivate();
        Entities.Remove(id);
        
        if (PhotonNetwork.IsMasterClient)
            GameServices.Inst.EventManager.Raise(NetworkEvent.EntityDestroied, id);
    }

    /// <summary>
    /// Vytváří entitu. Podle role vybere jak ji spáwnout. 
    /// Master: zavolá SpawnEntity a předá patřičná data.
    /// Regular: vyvolá event a po masterovi žádá a spawnutí entity, ten odpoví, že spawnul entitu
    /// </summary>
    /// <param name="data">Data které definují entitu</param>
    public void CreateEntity(EntityData data)
    {
        if (PhotonNetwork.IsMasterClient)
            SpawnEntity(data);
        else
            GameServices.Inst.EventManager.Raise(NetworkEvent.EntityDemanded, data, ReceiverGroup.MasterClient);
    }
    /// <summary>
    /// Podle role vybere jak entitu vytvoří.
    /// Master: zavolá SpawnEntity a předá patřičná data.
    /// Regular: vyvolá event a po masterovi žádá a spawnutí entity, ten odpoví, že spawnul entitu
    /// </summary>
    /// <param name="data">Data které definují entitu</param>
    /// <param name="callback">Callback jakmile se entita úspěšně spawne</param>
    public void CreateEntity(EntityData data, Action<Entity> callback)
    {
        var ticket = UnityEngine.Random.Range(1, 10000000);
        data.Ticket = ticket;

        ticketCallBacks.Add(ticket, (entity) => { callback(entity); ticketCallBacks.Remove(ticket); });

        CreateEntity(data);
    }

    /// <summary>
    /// Vytvoří entitu na základě dat. 
    /// Vyjme z poolu volnou instanci, nastaví jí podle dat, obecně inicializuje a přidá do lokálního seznamu aktivních entit.
    /// Podle role se zachová a inicializuje patřičnou logiku.
    /// Případně zavolá callback pokuď byl nalezen.
    /// </summary>
    /// <param name="data">Data které definují entitu</param>
    void SpawnEntity(EntityData data)
    {
        var entity = AppServices.Inst.EntityPool.GetEntity(data.Type);

        if(entity == null)
            return;
        
        entity.transform.position = data.InitialPosition.Vector;
        entity.transform.rotation = data.InitialRotation.Quaternion;

        entity.Team = data.Team;
        entity.Type = data.Type;

        data.ID = entity.ID = (data.ID != -1 ? data.ID : entity.GetUniqueID());

        entity.Initialize();
        

        Entities.Add(entity.ID, entity);
        
        //TODO: separate?
        if (PhotonNetwork.IsMasterClient)
        {
            GameServices.Inst.EventManager.Raise(NetworkEvent.EntityCreated, data);
            entity.MasterLogic();
        }
        else
            entity.RegularLogic();

        if (data.Ticket != -1 && ticketCallBacks.ContainsKey(data.Ticket))
            ticketCallBacks[data.Ticket](entity);
    }
}

/// <summary>
/// Data definující entitu
/// </summary>
[Serializable]
public class EntityData
{
    public EntityData()
    {
        InitialPosition = new V3(Vector3.zero);
        InitialRotation = new Q(Quaternion.identity);
    }

    public EntityType Type;
    public Team Team;
    public int ID = -1;
    public V3 InitialPosition;
    public Q InitialRotation;
    public int Ticket = -1;
}

/// <summary>
/// Serializovatelná verze Quaternion, je zapotřebí pro použítí po sítí s Photonem.
/// </summary>
[Serializable]
public class Q
{
    public float X, Y, Z, W;

    public Q(Quaternion q)
    {
        Quaternion = q;
    }

    public Quaternion Quaternion {
        get {
            return new Quaternion(X, Y, Z, W);
        }
        set {
            X = value.x;
            Y = value.y;
            Z = value.z;
            W = value.w;
        }
    }
}

/// <summary>
/// Serializovatelná verze Vector, je zapotřebí pro použítí po sítí s Photonem.
/// </summary>
[Serializable]
public class V3
{
    public float X, Y, Z;

    public V3(Vector3 v3)
    {
        Vector = v3;
    }

    public Vector3 Vector {
        get {
            return new Vector3(X, Y, Z);
        }
        set {
            X = value.x;
            Y = value.y;
            Z = value.z;
        }
    }
}
