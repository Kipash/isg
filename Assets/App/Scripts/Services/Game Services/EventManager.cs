﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

/// <summary>
/// Posílá a příma eventy skrze Photon.
/// 
/// Registrují se callbacky na určité ID eventu a po obdržení daného eventu se podle ID zavolá registrovaný callback.
/// </summary>
[Serializable]
public class EventManager : IInitializable
{
    /// <summary>
    /// Callback na eventy, NetworkEvent je ID a Action<object> je ten samotný callback, který musí porozumět parametru a přetypovat ho.
    /// </summary>
    public Dictionary<NetworkEvent, Action<object>> Processors = new Dictionary<NetworkEvent, Action<object>>();

    /// <summary>
    /// Obalení pro registraci Photonovského eventu OnEvent.
    /// </summary>
    PhotonEvents photonEvents;

    /// <summary>
    /// Registruje OnEvent, aby se volal při přijatí eventu.
    /// </summary>
    /// <param name="instance">instace GameServices</param>
    public void Initialize(object instance)
    {
        photonEvents = new PhotonEvents(OnEvent);
        PhotonNetwork.AddCallbackTarget(photonEvents);
    }
    
    /// <summary>
    /// Volá se při obdržení eventu.
    /// 
    /// Zkontroluje se ID, jeho obsah a poté se zavolá patřičný callback.
    /// </summary>
    /// <param name="photonEvent"></param>
    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code > 199)
            return;

        NetworkEvent eventID = NetworkEvent.none;

        try
        {
            eventID = (NetworkEvent)photonEvent.Code;
        } catch { }

        if (eventID == NetworkEvent.none)
            return;

        var hs = (ExitGames.Client.Photon.Hashtable)photonEvent.CustomData;
        var data = ByteArrayToObject((byte[])hs["data"]);

        //process
        Log.Write(Log.Dye("Event: ", 5) + " >>> " +
                  "ID: " + Log.Dye(eventID.ToString(), 6) + " /// " +
                  "actor: " + Log.Dye(photonEvent.Sender.ToString(), 4));

        if(Processors.ContainsKey(eventID))
        {
            Processors[eventID].Invoke(data);
        }
    }
    
    /// <summary>
    /// Posílá event, wrapper pro PhotonNetwork.RaiseEvent
    /// </summary>
    /// <param name="id">Položka z NetwrokEvent enumu.</param>
    /// <param name="data">Jakákoliv instance třídy s atributem [Serializable]. Také může být číslo a systémové typy.</param>
    /// <param name="receiverGroup"></param>
    public void Raise(NetworkEvent id, object data, ReceiverGroup receiverGroup = ReceiverGroup.Others)
    {
        var eventOptions = new RaiseEventOptions() { Receivers = receiverGroup };
        var sendOptions = new SendOptions() { Reliability = true };
        
        var hs = new ExitGames.Client.Photon.Hashtable();
        hs.Add("data", ObjectToByteArray(data));

        PhotonNetwork.RaiseEvent((byte)id, hs, eventOptions, sendOptions);
    }

    /// <summary>
    /// Serializace objektu na binárku
    /// </summary>
    /// <param name="obj">instance třídy</param>
    /// <returns></returns>
    byte[] ObjectToByteArray(object obj)
    {
        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream ms = new MemoryStream())
        {
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }
    }
    /// <summary>
    /// Deserializace objektu z binárku na instanci
    /// </summary>
    /// <param name="data">syrová binárka</param>
    /// <returns></returns>
    object ByteArrayToObject(byte[] data)
    {
        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream ms = new MemoryStream(data))
        {
            return bf.Deserialize(ms);
        }
    }
}