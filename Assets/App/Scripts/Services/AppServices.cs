﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using Photon.Pun;
using System.Text.RegularExpressions;
using Backend;

/// <summary>
/// Hlavní hub, který má existovat po celou dobu aplikace.
/// 
/// GlobalNetwork - Zprostředkovává připojení k Photonu.
/// Matchmaking - Zprostředkovává logiku pro připojení do lobby (Photon)
/// EntityPool - Vytváří entity do zásoby na startu.
/// </summary>
public class AppServices : MonoBehaviour
{
    static AppServices currentInstance;
    public static AppServices Inst { get { return currentInstance; } }


    public GlobalNetwork GlobalNetwork;
    public Matchmaking Matchmaking;
    public EntityPool EntityPool;

    [Header("Skip menu")]
    [SerializeField] bool skipMenu;

    [Header("Halt")]
    [SerializeField] bool haltOnStart;
    bool isReady;

    [Header("Log")]
    [SerializeField] bool enableLog;
    [SerializeField] DevLog developerLog;
    
    void Awake()
    {
        CreteInstance();

        InitializeDataStorage();

        //Log
        if (enableLog)
        {
            Log.AddListener(new UnityLog());
            Log.AddListener(developerLog);
        }

        InitializeServices();
    }
    
    void Start()
    {
        //Temp name definition, TODO: remove
        PhotonNetwork.LocalPlayer.NickName = "Dev player(" + UnityEngine.Random.Range(10, 100) + ")";

        //Temp development option, TODO: remove
        if (haltOnStart)
            GlobalNetwork.Ready += () => { isReady = true; Debug.Log("Press SPACE to proceed"); };
        else //Temp way of loading scenes, TODO: remove
            GlobalNetwork.Ready += () => { Application.LoadLevel(skipMenu ? 2 : 1); };
            
        //Connect to Photon
        GlobalNetwork.ConnectToMaster();
    }

    //Temp development option, TODO: remove
    private void Update()
    {
        if(haltOnStart && isReady && Input.GetKeyDown(KeyCode.Space))
            Application.LoadLevel(skipMenu ? 2 : 1);
    }

    string GetUserDataPath()
    {
        string path;

        path = Application.persistentDataPath + "/userdata";

        if (SystemInfo.operatingSystem != null && SystemInfo.operatingSystem.StartsWith("Windows") &&
            !string.IsNullOrEmpty(Environment.CommandLine))
        {
            var idArg = Regex.Match(Environment.CommandLine, @"--id=(\w+)");
            if (idArg.Success)
                path += "-" + idArg.Groups[1].Value;
        }

        return path;
    }

    void InitializeDataStorage()
    {
        var userDatapath = GetUserDataPath();
        var programDataText = Resources.Load<TextAsset>("Program-Data").text;
        DataStorage.Load(userDatapath, programDataText);
    }

    void CreteInstance()
    {
        if (Inst == null)
        {
            currentInstance = this;
            Log.Write("Setting up AppServices ...", 4);
        }
        else
        {
            Log.Write("err: AppServices already exist, destroying ...", 1);
            Destroy(gameObject);
        }
    }
    void InitializeServices()
    {
        var services = this.GetType()
                           .GetFields()
                           .Where(x => x.GetValue(this) is IInitializable)
                           .Select(x => x.GetValue(this));
        
        foreach(IInitializable s in services)
        {
            s.Initialize(Inst);
            Log.Write("> " + s.GetType().Name + " initialized", 3);
        }
    }

    void OnDestroy()
    {
        currentInstance = null;
    }
}