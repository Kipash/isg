﻿using UnityEngine.XR;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Hlavní hub pro všechny singletony pro hru (scene: Main.unity). 
/// Pokud AppServices jsou null, tak se automaticky prepne na Scene s indexem 0, coz je PreScene kde se AppServices inicializujou.
/// 
/// RoomNetowrk - Photon eventy co se dějí v rámci místnosti, např. OnPlayerLeftRoom.
/// EventManager - Sprostředkovává přijmání a odesílání eventů přes Photon.
/// MapManager - Generuje mapu a dělá operace pro naleznutí tilu na určité pozici.
/// GameManager - Drží manu a pomáha zpustit hru, měl by držet stav hry a vše co je s ní spojeno.
/// ControlsManager - Přepína mezi PC a VR ovládaním.
/// VRPlayerManager - Spravuje 3D avatry lokálního a cizího hráče.
/// </summary>
public class GameServices : MonoBehaviour
{
    static GameServices currentInstance;
    public static GameServices Inst { get { return currentInstance; } }

    public RoomNetwork RoomNetwork;
    public EventManager EventManager;
    public EntityManager EntityManager;
    //public MapHexManager MapHexManager;
    //public MapMeshManager MapMeshManager;
    public GameManager GameManager;
    public VRPlayerManager VRPlayerManager; //TODO: rename?
    public ControlsManager ControlsManager;

    public IControls Controls;

    List<IUpdateable> updateables = new List<IUpdateable>();
    List<ILateUpdateable> lateUpdateables = new List<ILateUpdateable>();

    void Awake()
    {
        //TODO: change
        if(AppServices.Inst == null)
        {
            if(Application.loadedLevel != 0)
                Application.LoadLevel(0);
            return;
        }
        
        AppServices.Inst.Matchmaking.OnJoinedRoom += (x) =>
        {
            CreteInstance();
            InitializeServices();
        };

        //Mock
        if (AppServices.Inst.GlobalNetwork.IsReady)
            AppServices.Inst.Matchmaking.JoinOrCreate("Dev room - the one and only");
    }
    
    void Update()
    {
        foreach(var u in updateables)
        {
            u.Update();
        }
    }

    private void LateUpdate()
    {
        foreach (var u in lateUpdateables)
        {
            u.LateUpdate();
        }
    }

    void CreteInstance()
    {
        if (Inst == null)
        {
            currentInstance = this;
            Log.Write("Setting up GameServices ...", 4);
        }
        else
        {
            Log.Write("err: GameServices already exist, destroying ...", 1);
            Destroy(gameObject);
        }
    }
    void InitializeServices()
    {
        var initializeableservices = this.GetType()
                           .GetFields()
                           .Where(x => x.GetValue(this) is IInitializable)
                           .Select(x => x.GetValue(this));

        foreach (IInitializable s in initializeableservices)
        {
            s.Initialize(Inst);
            Log.Write("> " + s.GetType().Name + " initialized", 3);
        }

        var updatableServices = this.GetType()
                           .GetFields()
                           .Where(x => x.GetValue(this) is IUpdateable)
                           .Select(x => x.GetValue(this));

        foreach (IUpdateable s in updatableServices)
        {
            updateables.Add(s);
            Log.Write("> " + s.GetType().Name + "  added to Update", 3);
        }

        var lateUpdatableServices = this.GetType()
                   .GetFields()
                   .Where(x => x.GetValue(this) is ILateUpdateable)
                   .Select(x => x.GetValue(this));

        foreach (ILateUpdateable s in lateUpdatableServices)
        {
            lateUpdateables.Add(s);
            Log.Write("> " + s.GetType().Name + "  added to LateUpdate", 3);
        }
    }

    void OnDestroy()
    {
        currentInstance = null;
    }
}
