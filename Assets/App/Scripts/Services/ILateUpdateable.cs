﻿public interface ILateUpdateable
{
    void LateUpdate();
}