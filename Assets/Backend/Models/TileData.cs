﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Backend
{
    public class TileData
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Depth { get; set; }

    }
}