﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Backend
{
    public class MapData
    {
        public int Height { get; set; }
        public int Width { get; set; }

        public List<TileData> Tiles { get; set; }
    }
}