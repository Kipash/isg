﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Backend
{
    public static class DataStorage
    {
        static string userPath;

        public static UserData UserData { get; private set; }
        public static ProgramData ProgramData { get; private set; }

        
        public static void Load(string userDataPath, string programDataXml)
        {
            userPath = userDataPath;
            UserData = LoadUserData();
            ProgramData = LoadProgramData(programDataXml);
        }
        public static void SaveUserData()
        {
            if (UserData == null)
                throw new NullReferenceException("UserData cannot be null");

            Save(userPath, UserData);
            
        }

        static UserData LoadUserData()
        {
            var data = LoadFromFile<UserData>(userPath);
            
            if (data == null)
                data = new UserData();

            /*
            if (data.Products == null)
                data.Products = new List<ProductUserData>();
            if (data.Options == null)
                data.Options = new Options();
            if (data.UserName == null)
                data.UserName = "";
            if (data.SelectedProducts == null)
                data.SelectedProducts = new int[0];
            */

            return data;
        }
        static ProgramData LoadProgramData(string xml)
        {
            var data = LoadFromText<ProgramData>(xml);
            
            if (data == null)
                data = new ProgramData();
            if(data.MapData == null)
            {
                data.MapData = new MapData();
                data.MapData.Tiles = new List<TileData>();
            }
            /*
            if (data.Products == null)
                data.Products = new ProductProgramData[0];
            if (data.DefaultProducts == null)
                data.DefaultProducts = new int[0];
            */

            return data;
        }
                
        public static void Save<T>(string path, T obj) where T : class, new()
        {
            using (var sw = new StreamWriter(path))
            {
                var xs = new XmlSerializer(typeof(T));
                xs.Serialize(sw, obj);
            }
        }
        public static T LoadFromFile<T>(string path) where T : class, new()
        {
            if (File.Exists(path))
            {
                using (var sr = new StreamReader(path))
                {
                    var xs = new XmlSerializer(typeof(T));
                    return (T)xs.Deserialize(sr);
                }
            }
            return new T();
        }
        public static T LoadFromText<T>(string value) where T : class, new()
        {
            if (!string.IsNullOrEmpty(value))
            {
                using (var sr = new MemoryStream(Encoding.UTF8.GetBytes(value)))
                {
                    var xs = new XmlSerializer(typeof(T));
                    return (T)xs.Deserialize(sr);
                }
            }
            return new T();
        }
    }
}
