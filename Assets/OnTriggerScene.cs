﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class OnTriggerScene : MonoBehaviour 
{
    [Header("Temporarly")]
    [SerializeField] int sceneIndex;
    [SerializeField] SteamVR_Action_Boolean trigger;

    private void OnTriggerStay(Collider other) 
    {
        if(trigger.GetState(SteamVR_Input_Sources.Any)) 
        {
            Application.LoadLevel(sceneIndex);
        }
    }
}
